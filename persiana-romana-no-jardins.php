<?php
$title       = "Persiana Romana no Jardins";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conhecida como Persiana Romana no Jardins, é uma persiana com acionamento por manivela, motor ou corrente, indicada para decorar praticamente qualquer ambiente de sua casa. Parecida com a persiana rolô, se difere pela textura formada quando a persiana do modelo romana é recolhida, formando gomos e divisórias no tecido e proporcionando um design único e luxuoso. Encontre com a Maliete Decorações.</p>
<p>Com a Maliete Decorações proporcionando de forma excelente Cortina de Tecido, Persiana preta, Loja de Persiana Motorizada, Cortinas para Sala Preço e Manutenção de Persianas conseguindo manter a alta qualidade e credibilidade no ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, assim, consequentemente, proporcionando o que se tem de melhor em resultados para você. Possibilitando diversas escolhas para os melhores resultados, a nossa empresa torna-se referência com Persiana Romana no Jardins.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>