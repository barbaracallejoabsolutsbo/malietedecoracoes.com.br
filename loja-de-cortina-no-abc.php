<?php
    $title       = "Loja de Cortina no ABC";
    $description = "Somos uma loja de cortina no ABC que possui os melhores preços e opções para atender da melhor forma nossos clientes, trazendo qualidade e excelência.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/".$url_title."/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/".$url_title."/".$url_title."-thumb.jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <h2>Você esta procurando por loja de cortina no ABC?</h2>
<p>A Maliete Decorações é a melhor opção de loja de cortina no ABC pra você.</p>
<p>Loja de cortina no ABC com os melhores preços!</p>
<p>Loja de cortina no ABC com qualidade, elegância e garantia.</p>
<p>O uso de cortinas em ambientes está na história há muitos anos, mas ainda hoje temos duvidas de quando e como podemos usá-las.</p>
<p>Recomenda-se padronizar as cortinas em ambientes com várias janelas. Mas, se o ambiente for banheiros e cozinhas, recomenda-se utilizar persianas, que são mais fáceis de limpar, detalhe fundamental para ambientes expostos a gordura e umidade.</p>
<p>Quando falamos em loja de cortina no ABC, precisamos saber a diferença entre persiana e cortina, já que embora tenham praticamente a mesma função, se diferenciam em muitos aspectos. As cortinas são feitas a partir de tecidos com uma forma muito mais artesanal, podendo ser combinados com itens decorativos e tendo um caimento completo.</p>
<p>Garantir privacidade, controlar a luminosidade, proteger o mobiliário do excesso de sol e proporcionar conforto térmico dentro da casa são umas das funções das cortinas.</p>
<p>Loja de cortina no ABC com infinitas possibilidades de escolha em toda a linha de cortinas e persianas, modelos de altíssima qualidade e enorme quantidade de cores.</p>
<p>Loja de cortina no ABC que permitem facilmente a aplicação de um motor nas persianas e cortinas que funciona a partir de controle remoto.</p>
<p>Em espaços maiores, as peças motorizadas fazem toda a diferença, já que o espaço tende a necessitar de cortinas ou persianas maiores.</p>
<p>Com a escolha do melhor produto para cada espaço, a luz torna-se uma valiosa ferramenta decorativa e contribui significativamente para a criação de um ambiente prazeroso e aconchegante. Com os produtos feitos na loja de cortina no ABC da nossa empresa, você terá a capacidade de dominar, controlar e até mesmo dar forma à luminosidade.</p>
<p>Loja de cortina no ABC é com a gente!</p>
<p>Entre em contato com a Maliete Decorações por telefone ou pelo nosso e-mail.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>