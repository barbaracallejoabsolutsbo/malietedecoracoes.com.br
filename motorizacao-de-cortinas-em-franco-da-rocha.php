<?php
$title       = "Motorização de Cortinas em Franco da Rocha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Motorização de Cortinas em Franco da Rocha e persianas é cada vez mais uma tendência que alia conforto, estética e economia. Se há uma palavra associada ao lar é definitivamente o conforto. Para facilitar o seu dia-a-dia é sempre um bom investimento tornar a sua casa o mais confortável possível. Conheça os benefícios e os tipos de Motorização de Cortinas em Franco da Rocha disponíveis com a Maliete Decorações, com mão de obra especializada.</p>
<p>Você procura por Motorização de Cortinas em Franco da Rocha? Contar com empresas especializadas no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maliete Decorações é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Motorização de Cortinas, Cortina para quarto, Loja de Papel de Parede para Banheiro, Loja de Cabeceira para Cama de Solteiro e Persiana Double Vision e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>