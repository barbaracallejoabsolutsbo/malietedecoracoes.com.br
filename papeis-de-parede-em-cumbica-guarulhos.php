<?php
$title       = "Papéis de Parede em Cumbica - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os Papéis de Parede em Cumbica - Guarulhos são opções de decoração cada vez mais procuradas atualmente. Com a facilidade na instalação é possível realizar a troca quantas vezes você quiser redecorar sem muito esforço. Além disso, os papeis de parede são grandes aliados principalmente de famílias que contam com crianças ou animais domésticos que constantemente colocam a pintura em risco. </p>
<p>Buscando por uma empresa de credibilidade no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, para que, você que busca por Papéis de Parede em Cumbica - Guarulhos, tenha a garantia de qualidade e idoneidade, contar com a Maliete Decorações é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Papel de parede para sala, Persiana Lumiére, Persiana para quarto, Loja de Persiana Motorizada e Reforma de Cortinas para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>