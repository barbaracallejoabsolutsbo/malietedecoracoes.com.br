<?php
$title       = "Cortina sob medida no Morumbi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você procura por uma loja que venda Cortina sob medida no Morumbi com a maior variedade para você escolher os modelos que mais lhe agradarem encontrou o lugar certo. A Maliete Decorações conta com a maior variedade de decorativos para você, venha conferir. São diversas opções de cortinas, persianas, papeis de parede e tapeçaria com alta qualidade disponibilizadas especialmente para você. </p>
<p>A Maliete Decorações, além de ser especializados em Loja de Papel de Parede para Banheiro, Modelo de Cortinas para Quarto, Persiana Romana, Loja de Cortinas Sob Medida e Cortinas, atuando no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias com a missão de oferecer sempre o melhor para seus clientes, de forma que seus objetivos sejam alcançados. Além disso, contamos com profissionais competentes visando prestar o melhor atendimento possível em Cortina sob medida no Morumbi para ser sempre a opção número um de nossos parceiros e clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>