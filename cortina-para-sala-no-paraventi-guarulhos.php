<?php
$title       = "Cortina para sala no Paraventi - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Dentre nossa grande variedade de cortinas você irá encontrar a que perfeitamente se enquadra na sua decoração seja residencial ou corporativa. Caso você tenha dúvidas sobre a Cortina para sala no Paraventi - Guarulhos ou quaisquer outros produtos oferecidos por nossa empresa entre em contato e seja atendido por um decorador de nossa equipe para te auxiliar a encontrar a cortina que você procura comprar. </p>
<p>Com uma ampla atuação no segmento, a Maliete Decorações oferece o melhor quando falamos de Cortina para sala no Paraventi - Guarulhos proporcionando aos seus clientes a máxima qualidade e desempenho em Persiana de madeira horizontal, Persiana para escritório, Cortinas de Varão, Cortina para quarto e Persiana para sala, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de  Cortinas, Persianas, Papel de Parede e Tapeçarias.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>