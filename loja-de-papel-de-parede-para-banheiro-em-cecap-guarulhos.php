<?php
$title       = "Loja de Papel de Parede para Banheiro em CECAP - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Maliete Decorações é uma Loja de Papel de Parede para Banheiro em CECAP - Guarulhos com diversas opções para você que procura renovar o visual do seu banheiro economizando dinheiro e garantindo qualidade. Com designs exclusivos, oferecemos padrões, texturas, cores e materiais diferentes para deixar o visual do banheiro de acordo com seu gosto pessoal. Acesse nosso site, fale conosco e obtenha mais informações.</p>
<p>Especialista no mercado, a Maliete Decorações é uma empresa que ganha visibilidade quando se trata de Loja de Papel de Parede para Banheiro em CECAP - Guarulhos, já que possui mão de obra especializada em Persiana preta, Papel de Parede para Lavabo, Papel de Parede Estampado, Reforma de Cortinas e Papéis de Parede. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>