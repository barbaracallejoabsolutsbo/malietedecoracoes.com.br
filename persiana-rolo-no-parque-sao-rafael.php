<?php
$title       = "Persiana Rolô no Parque São Rafael";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O modelo de Persiana Rolô no Parque São Rafael se difere dos demais modelos pela sua infinita possibilidade de decoração com fácil instalação, além de possuir alta eficiência ao controlar a entrada de iluminação pelos locais instalados, assim permitindo economizar energia e climatizar os ambientes de acordo com a necessidade. Encontre diversos modelos de persiana, cortina, painéis e papéis de parede com a Maliete Decorações. </p>
<p>Trabalhando para garantir Persiana Rolô no Parque São Rafael de maior qualidade no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, contar com a Maliete Decorações que, proporciona, também, Onde Comprar Canto Alemão, Persiana Painel, Cortina de voil, Persiana Personalizada e Persiana Melíade com a mesma excelência e dedicação se torna a melhor decisão para você. Se destacando de forma positiva das demais empresas, nós contamos com profissionais amplamente capacitados para um atendimento de sucesso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>