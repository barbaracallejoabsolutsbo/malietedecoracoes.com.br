<?php

    $title       = "Papel de Parede";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="topo-mobile">
            <div class="container">
                <div class="contato-atendimento">
                    <div class="icone">
                        <p class="btn-orcamento"><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><b>Faça seu Orçamento</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="topo-pags">
            <div class="container">
                <h1><?php echo $h1;?></h1>
                <hr>
            </div>
        </div>
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="todos-pags">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="autoplay">
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-11.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-11.jpg" class="img-responsive content-image" alt="papel-de-parede-linha-roll-in-stone" title="papel-de-parede-linha-roll-in-stone">
                                        <div class="content-details fadeIn-top">
                                            <h3>Papel de Parede Linha Roll in Stone</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-7.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-7.jpg" class="img-responsive content-image" alt="papel-de-parede-linha-livina" title="papel-de-parede-linha-livina">
                                        <div class="content-details fadeIn-top">
                                            <h3>Papel de Parede Linha Livina</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-21.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-21.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Papel de Parede Maliete</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-22.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-22.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Papel de Parede Maliete</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-23.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-23.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Papel de Parede Maliete</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>        
                    <div class="col-md-1">
                    </div>
                    <div class="cont-text col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <p>Os nossos papéis de paredes são da mais alta qualidade e vinílicos, tendo uma maior resistência. Em sua maioria são importados vindos da França e da Itália. Nós da Maliete Decorações temos dos mais variados tipos, estampados, lisos, infantis, brilhantes, de linho, mika e muito mais. Também contamos com uma equipe especializada para garantir o melhor acabamento.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/jquery.slick"
    )); ?>
    <script>
        $(function(){

            $(".autoplay").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 200000
            });

        });
    </script>
</body>
</html>