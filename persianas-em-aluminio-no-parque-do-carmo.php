<?php
$title       = "Persianas em Alumínio no Parque do Carmo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Persianas em Alumínio no Parque do Carmo são muito duráveis, higiênicas e de fácil manutenção. Feitas em material resistente e que bloqueia os raios solares, proporciona um ambiente com iluminação controlada e perfeitas para privacidade, modernidade e conforto. Com a Maliete Decorações você encontra persianas em modelos de alumínio, pvc, entre outros materiais, tecidos, cores e texturas. Conheça mais sobre nossos produtos.</p>
<p>A empresa Maliete Decorações possui uma ampla experiência no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, de onde vem atuando em Persianas em Alumínio no Parque do Carmo com dedicação e proporcionando os melhores resultados, garantindo sempre a excelência. Assim, vem se destacando de forma positiva das demais empresas do mercado que vem atuando com total empenho em Persiana Plissada, Loja de Cabeceira para Cama de Solteiro, Loja de Papel de Parede para Banheiro, Cabeceira para Cama de Casal e Persiana preta. Entre em contato conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>