<?php
    $title       = "Loja de Persiana e Cortina na Zona Leste SP ";
    $description = "A Maliete Decorações é uma loja de persiana e cortina na zona leste SP, tendo como objetivo oferecer excelentes produtos com rapidez na entrega e instalação.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/".$url_title."/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/".$url_title."/".$url_title."-thumb.jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <h2>Você esta pesquisando por loja de persiana e cortina na Zona Leste SP?</h2>
<p>A Maliete Decorações é a melhor quando falamos em loja de persiana e cortina na Zona Leste SP.</p>
<p>Nossa empresa trabalha no ramo de decorações com persianas, cortinas e papel de parede, tendo como objetivo oferecer excelentes produtos com rapidez na entrega e instalação. São anos de experiência no segmento!</p>
<p>Loja de persiana e cortina na Zona Leste SP com garantia, elegância e confiabilidade.<br />Quando falamos em loja de persiana e cortina na Zona Leste SP, precisamos saber a diferença entre persiana e cortina, já que embora tenham praticamente a mesma função, se diferenciam em muitos aspectos. As persianas são feitas com alumínio, plástico, madeira ou qualquer material de origem rústica. Já as cortinas são feitas a partir de tecidos com uma forma muito mais artesanal, podendo ser combinados com itens decorativos e tendo um caimento completo.</p>
<p>Loja de persiana e cortina na Zona Leste SP com infinitas possibilidades de escolha em toda a linha de cortinas e persianas, modelos de altíssima qualidade e enorme quantidade de cores.</p>
<p>Com a escolha do melhor produto para cada espaço, a luz torna-se uma valiosa ferramenta decorativa e contribui significativamente para a criação de um ambiente prazeroso e aconchegante. Com os produtos feitos na loja de persiana e cortina na Zona Leste SP da nossa empresa, você terá a capacidade de dominar, controlar e até mesmo dar forma à luminosidade.</p>
<h3>Loja de persiana e cortina na Zona Leste SP com os melhores preços da região!</h3>
<p><br />Loja de persiana e cortina na Zona Leste SP que permitem facilmente a aplicação de um motor nas persianas que funciona a partir de controle remoto. Em espaços maiores, as peças motorizadas fazem toda a diferença, já que o espaço tende a necessitar de cortinas ou persianas maiores.</p>
<p>Temos o diferencial de vender produto direto de nossa loja, oferecendo preços baixos e melhores condições por eliminar diversos intermediários.</p>
<p>Loja de persiana e cortina na Zona Leste SP é com a gente!</p>
<p>Oferecemos também o serviço de manutenção e higienização de cortinas e persianas.</p>
<p>Faça um orçamento com a Maliete Decorações através dos nossos telefones ou e-mail.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>