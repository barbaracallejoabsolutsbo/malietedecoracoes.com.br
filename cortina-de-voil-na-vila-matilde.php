<?php
$title       = "Cortina de voil na Vila Matilde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Procurando o melhor lugar para comprar uma Cortina de voil na Vila Matilde com grandes variedades de opções? Se essa é sua procura encontrou o lugar certo, seja bem-vindo a Maliete Decorações. Nossa empresa atua com cortinas, persianas, papeis de parede e tapeçaria há mais de 30 anos com qualidade o suficiente para nos elevar ao posto de referência dentro desse segmento. </p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações oferece a confiança e a qualidade que você procura quando falamos de Persiana Personalizada, Cortina de Forro e Voil, Persiana Rolo tela solar, Persiana para sacada e Loja de Papel de Parede para Banheiro. Ainda, com o mais acessível custo x benefício para quem busca Cortina de voil na Vila Matilde, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>