<?php
$title       = "Persiana Painel na Vila Andrade";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você ainda não conhece a Persiana Painel na Vila Andrade, este modelo é inovador e moderno que proporciona um design diferenciado para grandes vãos. Composta por painéis com larguras que se sobrepõem para cobrir por completo quando esticadas, são perfeitas para grandes janelas e painéis de vidro. Conheça os serviços especializados em decoração de interiores da Maliete Decorações. Fale conosco e conheça mais.</p>
<p>Você procura por Persiana Painel na Vila Andrade? Contar com empresas especializadas no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maliete Decorações é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Cabeceira para Cama King Size Preço, Papel de parede de linho, Persiana de madeira horizontal, Persiana Lumiére e Cortinas e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>