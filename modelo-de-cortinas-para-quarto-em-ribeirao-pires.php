<?php
$title       = "Modelo de Cortinas para Quarto em Ribeirão Pires";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre uma infinidade de Modelo de Cortinas para Quarto em Ribeirão Pires com a Maliete Decorações. Localizados em Guarulhos, oferecemos o que há de melhor em cortinas e persianas para decoração do seu lar ou escritório há mais de 30 anos. Conheça nossos modelos de cortinas e persianas para quarto, sala de estar, sala de tv, entre outros ambientes. Invista em conforto e versatilidade com nossas cortinas.</p>
<p>A empresa Maliete Decorações é destaque entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Modelo de Cortinas para Quarto em Ribeirão Pires do mercado. Ainda, possui facilidade com Persiana Personalizada, Papel de Parede para Lavabo, Onde Comprar Cortinas Blackout, Persiana Rolo tela solar e Cortina blecaute mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>