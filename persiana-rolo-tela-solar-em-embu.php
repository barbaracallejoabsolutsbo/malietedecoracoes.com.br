<?php
$title       = "Persiana Rolo tela solar em Embu";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Persiana Rolo tela solar em Embu é uma opção muito vantajosa para pessoas que queiram optar pelo momento e quantidade de luz que entra pela janela no ambiente. Esse modelo de persiana é utilizado em quartos, salas, escritórios e diversos outros ambientes. Com a proposta de ser enrolada essa categoria de produto necessita de pouquíssimo espaço para ser guardado quando não utilizado. </p>
<p>Especialista no mercado, a Maliete Decorações é uma empresa que ganha visibilidade quando se trata de Persiana Rolo tela solar em Embu, já que possui mão de obra especializada em Cortinas para Sala Preço, Persiana Personalizada, Persiana Passione, Persiana Rolo tela solar e Persiana Vertical. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>