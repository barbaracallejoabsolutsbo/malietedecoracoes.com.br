<?php
$title       = "Cortina de Tecido em Embu das Artes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao colocar cortinas em seu ambiente você opta por versatilidade. Cortinas são essenciais para ambientação e, em dias claros e ensolarados, protegem o ambiente dos raios UV, proporcionando um ambiente com iluminação agradável e controlada. Encontre a Cortina de Tecido em Embu das Artes perfeita para seu ambiente com a Maliete Decorações e tenha mão de obra especializada para serviços de instalação e manutenção.</p>
<p>Especialista no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Cortina de Tecido em Embu das Artes. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Manutenção de Persianas, Cortina de Forro e Voil, Cortina blecaute, Cortina blackout e Loja de Persiana Motorizada e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>