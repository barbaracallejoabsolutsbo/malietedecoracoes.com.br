<?php
$title       = "Cortina de Forro e Voil em Água Azul - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O forro voil é um tipo de tecido de microfibra que pode ser utilizado de ambos lados. Entre modelos mais tradicionais e modelos mais modernos. A Maliete Decorações oferece Cortina de Forro e Voil em Água Azul - Guarulhos em tamanhos diferentes, cores, texturas e acabamentos. Conheça nossos serviços e produtos para decoração de interior e contrate os serviços da Maliete Decorações para cortinas e persianas.</p>
<p>Como uma especialista em  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações se destaca dentre as demais empresas quando o assunto é Cortina de Forro e Voil em Água Azul - Guarulhos, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Loja de Cortinas Sob Medida, Papel de Parede para Lavabo, Manutenção de Persianas, Persiana Rolo tela solar e Persiana Romana de Teto. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>