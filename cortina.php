<?php

    $title       = "Cortinas";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="topo-pags">
            <div class="container">
                <h1><?php echo $h1;?></h1>
                <hr>
            </div>
        </div>
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="todos-pags">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="autoplay">
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-19.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-19.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Voi Leda</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-18.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-18.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Voil Leda</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-17.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-17.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de voil</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-16.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-16.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de voil liso com forro</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-12.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-12.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Voil</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-6.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-6.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Quarto em seda</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-5.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-5.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de voil com chale no quarto</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-4.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-4.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Sala voil liso e chale</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-2.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-2.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Linho</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-1.jpg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-1.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                        <div class="content-details fadeIn-top">
                                            <h3>Cortina de Linho</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="cont-text col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <p>Nós da Maliete Decorações oferecemos o melhor serviço em cortinas, desde o uso de materiais de primeira qualidade até a confecção a mão de todas elas. Feita por nós há mais de 30 anos sempre prezamos a pontualidade na entrega, a qualidade, a transparência no serviço e garantimos um produto com o melhor acabamento disponível no mercado.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/jquery.slick"
    )); ?>
    <script>
        $(function(){

            $(".autoplay").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 200000
            });

        });
    </script>
</body>
</html>