<?php
    $title       = "Motorização de cortinas";
    $description = "A Maliete trabalha com grande diferencial em qualidade e pontualidade dos produtos que entregamos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Quem opta pela <strong>motorização de cortinas</strong>, opta por custo x benefício. Sem o acionamento manual não há desgaste, assim, a manutenção precoce de sua cortina ou persiana é adiada por tempo saudável. Unindo conforto, estética, economia e bem-estar, a <strong>motorização de cortinas</strong>, persianas e toldos é uma tendência nos dias atuais. Ela pode ajudar a aperfeiçoar sua rotina e contribuir para o aconchego do seu lar, criando cenas e estilos de iluminação para o seu espaço.</p>
<p>As cortinas são objetos diretamente relacionados com a iluminação do ambiente além de sua função decorativa. A <strong>motorização de cortinas</strong> possui utilidade em qualquer cômodo, seja casa ou apartamento, ou até mesmo para um ambiente comercial com salas de escritório, por exemplo. Isso por que a <strong>motorização de cortinas</strong> leva praticidade e agilidade a todos os lugares, proporcionando mais conforto na hora de controlar a luminosidade.</p>
<p>A Maliete está há mais de 30 anos no mercado e tem como objetivo trazer a melhor qualidade não só na <strong>motorização de cortinas,</strong> mas em todos os tipos de serviços que incluem persianas, cortinas e papeis de parede, transformando seu lar. Nosso foco é proporcionar aos nossos clientes os melhores produtos de decoração interior, deixando seu espaço mais bonito e confortável.</p>
<p>Existem variados tipos de motorização para suprir as necessidades de cada cliente. O motor pode ter alimentação de bateria ou rede elétrica, sendo o acionamento feito por controle remoto ou interruptor. A <strong>motorização de cortinas</strong> vem para tornar sua rotina mais tranquila e descomplicada, sendo um bom investimento para contribuir com as tarefas do seu dia a dia sem você precisar se preocupar com a as janelas da sua casa.</p>
<h2>Conheça mais a respeito da <strong>motorização de cortinas</strong></h2>
<p>Os tipos de <strong>motorização de cortinas </strong>podem ser variados. Acionado por interruptor ou controle remoto, wi-fi ou motor a bateria – sendo este ultimo não necessária a utilização de energia elétrica. Cada um tem sua vantagem e facilidade, dessa forma você pode escolher o tipo que for mais útil para sua casa, para você e para sua rotina.</p>
<p>Com o mundo tecnológico de hoje, a <strong>motorização de cortinas </strong>pode ser feita pelo celular ou tablete, e ainda mais, você pode programar horários de abertura e fechamento das suas cortinas. Fácil, né? Com a <strong>motorização de cortinas </strong>você pode comandá-las e nem precisa estar por perto enquanto elas abrem ou fecham.</p>
<p>Além da facilidade mecânica, a <strong>motorização de cortinas </strong>acrescenta em aspectos estéticos e na vida útil dos produtos. O ambiente torna-se mais limpo e discreto, com ar moderno ou modesto, se adequando ao seu gosto e personalidade.</p>
<p>Ainda, a <strong>motorização de cortinas</strong> traz agilidade e requinte para ambientes empresariais e comerciais, eliminando correntes e fitas tradicionais comuns em produtos não motorizados.</p>
<p>No caso de salas de escritório, é comum que as paredes sejam de vidro. O fechamento motorizado das cortinas pode evitar que cada uma das persianas precisem ser manualmente abertas ou fechadas, poupando tempo e movimentação.</p>
<p>Em ambientes residenciais a <strong>motorização de cortinas </strong>facilita na hora de sair atrasado, por exemplo, evitando que o sol excessivo cause danos ou estragos nos móveis, pisos e estofados.</p>
<h2>As vantagens da <strong>motorização de cortinas</strong></h2>
<p>Investir na <strong>motorização de cortinas </strong>vai além da praticidade. Os benefícios integram praticidade e questões estéticas. Uma maneira de economizar em longo prazo é assegurar a vida útil de seus produtos, e a <strong>motorização de cortinas</strong> surge com essa proposta ao eliminar o acionamento manual e evitar seu desgaste.</p>
<p>Outra grande vantagem é a estética na <strong>motorização de cortinas</strong>. Sem cordas, correntes ou bastões, elas trazem um aspecto mais clean e modernizado para sua sala, quarto, escritório, ou qualquer ambiente em que estiver colocada. Há opções de <strong>motorização de cortinas</strong> de forma embutida, o que deixa as peças mais bonitas sem comprometer o visual.</p>
<p>Confira os benefícios dessa opção:</p>
<ul>
<li>         Mais durabilidade para o produto</li>
<li>         Tecnologia acessível</li>
<li>         Maior segurança com a programação do acionamento dos motores</li>
<li>         Design mais moderno</li>
<li>         Conforto térmico e visual</li>
<li>         Flexibilidade e facilidade</li>
</ul>
<p>Com essa tecnologia atual, adquirir a <strong>motorização de cortinas</strong> trará grandes vantagens para você, para sua equipe no trabalho ou para sua família. Se você circula diariamente entre salas e escritórios, priorizar o ambiente de trabalho e levar mais comodidade é uma forma de investir na produtividade e conforto geral.</p>
<p>Nós garantimos que não há melhor empresa de decorações de interiores que traga qualidade e preço justo nos produtos e serviços. Portanto, não hesite em nos procurar e consultar não só sobre a <strong>motorização de cortinas</strong>, mas todos os outros itens que estão em nosso catálogo e que possam te interessar.</p>
<p>A decoração de interiores vem para promover diferentes locais e consagrar o melhor aproveitamento do espaço. A proposta é levar aconchego, privacidade e proporciona melhores ambientes para o tipo de atividade que você deseja, seja para lazer – como assistir um filme em clima de cinema – como para criar uma melhor atmosfera na hora de dormir, e assim por diante.</p>
<p>Nossa equipe esta sempre estudando e se adaptando as mudanças dessa área, para que você, nosso cliente, receba o máximo de conforto e qualidade em nossos produtos. Trabalhamos para que os preços de nossos produtos sejam acessíveis e compatíveis com seu bolso, para que você possa adquiri-los no momento em que desejar e precisar.  </p>
<p>Atualmente estamos localizados em São Paulo, em dois locais de atendimento – Av. Timóteo Penteado, 4504 ou Rua Emília Marengo, 09 - e para melhor recebe-los estamos trabalhando com hora marcada. Nossos profissionais possuem experiência com todos os produtos de nosso portfólio. Nossas mercadorias e nossos serviços são feitos pensando no melhor para que sua sala, quarto, escritório, ou qualquer outro ambiente, seja um local com seu estilo e personalidade.</p>
<p>A Maliete trabalha com grande diferencial em qualidade e pontualidade dos produtos que entregamos. Além de qualidade diferenciada e garantia de um bom atendimento que você tanto merece. Quem trabalha com a gente faz parte da equipe, veste a camisa e está pronto para tirar suas dúvidas através de nossos meios de contato ou com atendimento presencial.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>