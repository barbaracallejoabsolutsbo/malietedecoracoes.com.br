<?php
$title       = "Persiana Lumiére em Franco da Rocha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você procura por uma Persiana Lumiére em Franco da Rocha de alta qualidade com preço justo encontrou o lugar certo para realizar sua compra. A Maliete Decorações atua há mais de 30 anos com cortinas, persianas, papeis de parede e tapeçaria de alto padrão. Não perca essa oportunidade de decorar ou redecorar seu ambiente com decorativos de muita qualidade com o preço que cabe no seu bolso. </p>
<p>Empresa pioneira no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é qualificada e experiente quando o assunto são Loja de Persiana Motorizada, Persiana Celular, Cortina para quarto, Onde Comprar Papel de Parede Estampado e Cortina de linho. Aqui você encontra soluções personalizadas para o que necessita e tem acesso ao que há de melhor e mais moderno para Persiana Lumiére em Franco da Rocha sempre com muita eficiência e qualidade para garantir a sua satisfação e sua fidelidade conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>