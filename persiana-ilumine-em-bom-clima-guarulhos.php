<?php
$title       = "Persiana Iluminê em Bom Clima - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Persiana Iluminê em Bom Clima - Guarulhos é uma categoria de persiana que permite a utilização e controle da luz natural de acordo com as preferências do usuário. As persianas geralmente são itens decorativos de muita utilidade e com as mais variadas categorias para que você escolha a que melhor atenda suas necessidades. Visite nosso site e confira os modelos disponíveis em nosso catalogo.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações ganha destaque por ser confiável e idônea quando falamos não só de Persiana Iluminê em Bom Clima - Guarulhos, mas também quando o assim é Persiana Double Vision, Cortina de linho, Papel de Parede para Lavabo, Persiana para quarto e Persiana Romana. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>