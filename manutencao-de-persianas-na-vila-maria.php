<?php
$title       = "Manutenção de Persianas na Vila Maria";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Com uma equipe treinada de instaladores e reparadores de persianas, a Maliete Decorações oferece o que há de mais moderno no mercado de persianas. Oferecemos Manutenção de Persianas na Vila Maria, sejam manuais ou motorizadas, assim como outros itens para decoração de ambientes internos de alta qualidade e acabamento impecável. Encontre este e outros serviços com nossa empresa de decoração de interiores.</p>
<p>Especialista no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Manutenção de Persianas na Vila Maria. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Persiana Rolô, Cabeceira para Cama King Size Preço, Persiana para escritório, Persiana para sala e Cabeceira de Cama Sob Medida e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>