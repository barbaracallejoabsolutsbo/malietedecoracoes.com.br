<?php
$title       = "Persiana Romana de Teto na Freguesia do Ó";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre diversos modelos de persiana com a Meliete Decorações, especializada em cortinas, papéis de paredes, tapeçarias e decoração para seu lar ou empresa. Oferecendo diversos opcionais para decorar seu espaço, encontre Persiana Romana de Teto na Freguesia do Ó com preço justo e muita agilidade. Encontre em cores, texturas e tecidos diversos somente aqui, na Meliete Decorações, decorações de interiores.</p>
<p>Com uma ampla atuação no segmento, a Maliete Decorações oferece o melhor quando falamos de Persiana Romana de Teto na Freguesia do Ó proporcionando aos seus clientes a máxima qualidade e desempenho em Papel de parede para o quarto, Cortina blecaute, Persiana para sala, Cortina de voil e Cabeceira de Cama Sob Medida, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de  Cortinas, Persianas, Papel de Parede e Tapeçarias.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>