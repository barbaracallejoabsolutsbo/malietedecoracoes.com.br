<?php
$title       = "Loja de Cabeceira para Cama de Solteiro no Jaguaré";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Maliete Decorações, localizada na cidade de Guarulhos no município de São Paulo, é uma Loja de Cabeceira para Cama de Solteiro no Jaguaré e de casal que oferece opcionais de confecção sob medida para você. Nossa ala de tapeçaria conta com profissionais experientes e atenciosos. Garantimos alta qualidade de produto, matéria prima e acabamento para todos produtos confeccionados pela Maliete Decorações.</p>
<p>Como uma empresa de confiança no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Loja de Cabeceira para Cama de Solteiro no Jaguaré. A Maliete Decorações vem crescendo e mostrando seu potencial através de Persianas em Alumínio, Persiana Iluminê, Papel de parede para o quarto, Persiana Vertical e Papel de parede de linho, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>