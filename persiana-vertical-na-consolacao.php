<?php
$title       = "Persiana Vertical na Consolação";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As persianas de PVC costumam proporcionar maior durabilidade e facilidade na limpeza, com a mesma beleza dos outros materiais. A Persiana Vertical na Consolação também pode ser feita em tecido translúcido, blackout ou tela solar, ou materiais mais sofisticados ainda. Conheça todos modelos disponíveis com a Maliete Decorações e escolha a melhor opção para compor seu ambiente. Fale conosco e obtenha mais informações.</p>
<p>Pensando em proporcionar a todos os clientes o melhor em métodos quando se trata de  Cortinas, Persianas, Papel de Parede e Tapeçarias? A empresa Maliete Decorações vem ganhando destaque em referência no assunto de Persiana Vertical na Consolação. Por isso, solicite um orçamento para Cortina de linho, Persiana Motorizada, Onde Comprar Canto Alemão, Reforma de Cortinas e Cabeceira para Cama King Size Preço assim você contará com a qualidade que somente a nossa empresa oferece para todos os seus clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>