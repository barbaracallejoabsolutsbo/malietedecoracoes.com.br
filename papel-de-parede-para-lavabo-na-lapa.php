<?php
$title       = "Papel de Parede para Lavabo na Lapa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para pequenos lavabos, as dicas são o uso de tons mais claros e suaves em papéis de parede como nas cores azul, verde, cinza, bege ou branco. Feitos em material vinílico, são resistentes a água e umidade, com acabamentos especiais. Encontre o Papel de Parede para Lavabo na Lapa adequado para sua necessidade com a Maliete Decorações. Tenha suporte total para decorações com cortinas, persianas e papéis de parede.</p>
<p>Nós da Maliete Decorações estamos entre as principais empresas qualificadas no ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias. Temos como principal missão realizar uma ótima assessoria tanto em Papel de Parede para Lavabo na Lapa, quanto à Persiana Blackout para quarto, Cortinas de Varão, Motorização de Cortinas, Persiana para sala e Cortinas de Trilho, uma vez que, contamos com profissionais qualificados e prontos para realizarem um ótimo atendimento. Entre em contato conosco e tenha a satisfação que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>