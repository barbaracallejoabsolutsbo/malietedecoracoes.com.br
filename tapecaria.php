<?php

    $title       = "Tapecaria";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="topo-mobile">
            <div class="container">
                <div class="contato-atendimento">
                    <div class="icone">
                        <p class="btn-orcamento"><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><b>Faça seu Orçamento</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="topo-pags">
            <div class="container">
                <h1>Tapeçaria</h1>
                <hr>
            </div>
        </div>
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="todos-pags">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="autoplay">
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-24.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-24.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Tapeçaria Maliate</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-25.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-25.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Tapeçaria Maliate</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-26.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-26.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Tapeçaria Maliate</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-27.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-27.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Tapeçaria Maliate</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="lista-galeria-fancy">
                                <div class="content">
                                    <a href="<?php echo $url; ?>imagens/produtos/produtos-28.jpeg">
                                        <div class="content-overlay"></div>
                                        <img src="<?php echo $url; ?>imagens/produtos/produtos-28.jpeg" class="img-responsive content-image" alt="imagem" title="imagem">
                                        <div class="content-details fadeIn-top">
                                            <h3>Tapeçaria Maliate</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="cont-text col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <p>Nossa ala de tapeçaria conta com canto alemão e cabeceiras. Nossa oficina possuí profissionais com mais de 20 anos de experiência no ramo que garantem o acabamento e a melhor qualidade do mercado. </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/jquery.slick"
    )); ?>
    <script>
        $(function(){

            $(".autoplay").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 200000
            });

        });
    </script>
</body>
</html>