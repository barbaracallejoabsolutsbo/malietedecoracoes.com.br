<?php
$title       = "Cortinas de Varão em Itapecerica da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os varões para cortinas estão cada vez mais sofisticados, oferecendo não só praticidade, mas também elegância e sofisticação à sua casa. A instalação de Cortinas de Varão em Itapecerica da Serra é muito fácil, podendo ser simples ou dupla, o que possibilita a colocação de tecidos translúcidos, opacos e inclusive blackouts também. Conheça uma diversidade de opções com a Maliete Decorações, especializada em decorações de interiores.</p>
<p>Trabalhando há anos no mercado voltado à  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é uma empresa de grande experiente e qualificada quando se trata de Papel de parede de linho, Reforma de Cortinas, Persiana Motorizada Preço, Cabeceira para Cama de Casal e Loja de Persiana Motorizada e Cortinas de Varão em Itapecerica da Serra. Em constante busca da satisfação de nossos clientes, atuamos no mercado com o intuito de fornecer para todos que buscam pela nossa qualidade o que há de mais moderno e eficiente no segmento.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>