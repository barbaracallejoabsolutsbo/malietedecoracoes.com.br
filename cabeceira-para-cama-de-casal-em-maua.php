<?php
$title       = "Cabeceira para Cama de Casal em Mauá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Entre os materiais mais comuns para cabeceira de cama estão o PVC, madeira e tapeçaria com estofado. A Maliete Decorações possui produtos e serviços especiais para você que procura por Cabeceira para Cama de Casal em Mauá. Conheça mais sobre nossa empresa e nossos serviços especializados para decoração de interiores que vão desde papéis de parede em diversos modelos a cortinas e persianas com designs exclusivos.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Maliete Decorações ganha destaque quando o assunto é  Cortinas, Persianas, Papel de Parede e Tapeçarias. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Cabeceira para Cama de Casal em Mauá até Loja de Cabeceira para Cama de Solteiro, Persiana Passione, Onde Comprar Papel de Parede Estampado, Onde Comprar Cortinas Blackout e Cortina de Forro e Voil com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>