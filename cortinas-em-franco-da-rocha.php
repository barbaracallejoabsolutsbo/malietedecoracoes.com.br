<?php
$title       = "Cortinas em Franco da Rocha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A maior variedade de Cortinas em Franco da Rocha para você encontrar as que mais agradem o seu gosto e atendam as suas necessidades estão aqui na Maliete Decorações. Nossa loja está há mais de 30 anos no mercado atendendo clientes dos mais diversos gostos para decoração com qualidade, compromissos e agilidade. Nossa longa estrada em conjunto com as boas avaliações nos garante ao posto de referência no segmento. </p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Cortinas em Franco da Rocha? A Maliete Decorações é o que você precisa! Sendo uma das principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Persiana Double Vision, Loja de Cabeceira para Cama de Solteiro, Cortina para quarto, Cortinas Romanas e Cortina blecaute. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>