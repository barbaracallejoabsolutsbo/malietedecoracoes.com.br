<?php
$title       = "Cortinas de Varão em Higienópolis";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Entre os principais benefícios em optar por instalar Cortinas de Varão em Higienópolis está a praticidade em remover as cortinas para lavagem, escolha de variados tecidos com possibilidade de troca, personalização, entre outras facilidades. Encontre uma infinidade de opções com a Maliete Decorações. Oferecemos serviços de instalação, reparo e reforma de cortinas em geral, persianas e papéis de parede, confira.</p>
<p>Se está procurando por Cortinas de Varão em Higienópolis e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Maliete Decorações é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Cortinas Romanas, Persiana Personalizada, Loja de Cortinas Sob Medida, Papel de Parede Infantil e Persiana Vertical.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>