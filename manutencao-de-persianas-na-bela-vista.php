<?php
$title       = "Manutenção de Persianas na Bela Vista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Maliete Decorações é especializada em cortinas, persianas, papéis de paredes e tapeçaria para decoração de interiores. Com diversos serviços que vão desde a venda, projeto e instalação destes itens aqui encontrados, oferecemos também Manutenção de Persianas na Bela Vista, com mão de obra especializada, agilidade e preço acessível. Consulte-nos agora mesmo e conheça mais sobre serviços e produtos conosco.</p>
<p>Como uma empresa especializada em  Cortinas, Persianas, Papel de Parede e Tapeçarias proporcionamos sempre o melhor quando falamos de Cortina blackout, Papel de Parede para Lavabo, Onde Comprar Papel de Parede Estampado, Cortina de Forro e Voil e Papel de parede para o quarto. Com potencial necessário para garantir qualidade e excelência em Manutenção de Persianas na Bela Vista com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Maliete Decorações trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>