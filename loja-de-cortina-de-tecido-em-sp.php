<?php
    $title       = "Loja de Cortina de Tecido em SP";
    $description = "Tendo como principal objetivo satisfazer nossos clientes, somos uma loja de cortina de tecido em SP que oferece atendimento personalizado e produtos de qualidade com garantia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/".$url_title."/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/".$url_title."/".$url_title."-thumb.jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <h2>Você esta procurando por loja de cortina de tecido em SP?</h2>
<p>A Maliete Decorações é a melhor opção pra você que procura por loja de cortina de tecido em SP.</p>
<p>Loja de cortina de tecido em SP com garantia, elegância e qualidade.</p>
<p>Garantir privacidade, controlar a luminosidade, proteger o mobiliário do excesso de sol e proporcionar conforto térmico dentro da casa são umas das funções das cortinas.</p>
<p>Nossa empresa trabalha no ramo de decorações com persianas, cortinas e papel de parede, tendo como objetivo oferecer excelentes produtos com rapidez na entrega e instalação.</p>
<p>Loja de cortina de tecido em SP com os melhores preços!</p>
<p>O uso de cortinas em ambientes está na história há muitos anos, mas ainda hoje temos duvidas de quando e como podemos usá-las.</p>
<p>Quando falamos em loja de cortina de tecido em SP, precisamos saber a diferença entre persiana e cortina, já que embora tenham praticamente a mesma função, se diferenciam em muitos aspectos. As cortinas são feitas a partir de tecidos com uma forma muito mais artesanal, podendo ser combinados com itens decorativos e tendo um caimento completo.</p>
<p>Loja de cortina de tecido em SP com infinitas possibilidades de escolha em toda a linha de cortinas e persianas, modelos de altíssima qualidade e enorme quantidade de cores.</p>
<p>Com a escolha do melhor produto para cada espaço, a luz torna-se uma valiosa ferramenta decorativa e contribui significativamente para a criação de um ambiente prazeroso e aconchegante. Com os produtos feitos na loja de cortina de tecido em SP da nossa empresa, você terá a capacidade de dominar, controlar e até mesmo dar forma à luminosidade.</p>
<p>Loja de cortina de tecido em SP que permitem facilmente a aplicação de um motor nas persianas e cortinas que funciona a partir de controle remoto. Em espaços maiores, as peças motorizadas fazem toda a diferença, já que o espaço tende a necessitar de cortinas ou persianas maiores.</p>
<p>A Maliete Decorações tem fábrica própria, por isso oferecemos preços baixos e melhores condições.</p>
<p>Loja de cortina de tecido em SP é com a Maliete Decorações!</p>
<p>Faça um orçamento com a Maliete Decorações através dos nossos telefones ou e-mail.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>