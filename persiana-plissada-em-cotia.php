<?php
$title       = "Persiana Plissada em Cotia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As persianas são objetos que podem ser utilizados juntos de cortinas para decoração e conforto. Com ampla variedade de cores, texturas e tecidos, as persianas podem ser encontradas em diversos modelos, entre eles: persiana rolô, persiana romana, Persiana Plissada em Cotia, persiana vertical, persiana painel, entre outros. A Maliete Decorações há mais de 30 anos no mercado oferece cortinas, persianas e etc.</p>
<p>Nós da Maliete Decorações trabalhamos dia a dia para garantir o melhor em Persiana Plissada em Cotia e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Cortina sob medida, Persiana Iluminê, Cortina para sala, Persiana de madeira horizontal e Papel de Parede Estampado e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>