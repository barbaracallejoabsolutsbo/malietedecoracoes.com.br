<?php
$title       = "Persiana Horizontal em Ponte Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você precisa de uma Persiana Horizontal em Ponte Grande - Guarulhos mas não encontra nenhum modelo que te agrade conheça as opções disponíveis na Maliete Decorações. Nossa empresa atua dentro do ramo há mais de 30 anos com qualidade, compromisso e alto padrão de atendimento. Contamos também com diversos outros tipos de persianas, cortinas, papeis de para e tapeçaria para sua decoração. </p>
<p>Desempenhando uma das melhores assessorias do segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Persiana Horizontal em Ponte Grande - Guarulhos com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Papéis de Parede, Persiana de madeira horizontal, Loja de Papel de Parede para Banheiro, Modelo de Cortinas para Quarto e Persiana preta, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>