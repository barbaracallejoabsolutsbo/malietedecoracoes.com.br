<?php
$title       = "Cortina para quarto em Guaianazes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está buscando por uma Cortina para quarto em Guaianazes que se encaixe perfeitamente na decoração de sua casa encontrou o lugar certo. A Maliete Decorações conta com uma grande variedade de cortinas para você encontrar a que perfeitamente se adequá ao seu gosto. Visite nosso site e confira o grande catálogo de variedades disponibilizados pensando especialmente em você. </p>
<p>Como uma especialista em  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações se destaca dentre as demais empresas quando o assunto é Cortina para quarto em Guaianazes, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Loja de Cabeceira para Cama de Solteiro, Persiana para quarto, Persiana Melíade, Persianas em Alumínio e Persiana Motorizada para Escritório. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>