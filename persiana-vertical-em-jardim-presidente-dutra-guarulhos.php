<?php
$title       = "Persiana Vertical em Jardim Presidente Dutra - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A persiana do modelo vertical é formada por lâminas verticais que são fixadas na parte superior de janelas, portas e painéis de vidro. Indicadas para escritórios, salas de estar e quartos, são recolhidas por diversas formas de acionamento, manual ou motorizada, oferecendo design e visual requintado. Encontre Persiana Vertical em Jardim Presidente Dutra - Guarulhos em diversos modelos de cor, tecido, textura, entre outros opcionais, com a Maliete Decorações.</p>
<p>A empresa Maliete Decorações é destaque entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Persiana Vertical em Jardim Presidente Dutra - Guarulhos do mercado. Ainda, possui facilidade com Onde Comprar Papel de Parede Estampado, Persiana Iluminê, Persiana Romana, Persiana Romana de Teto e Persiana Passione mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>