<?php
$title       = "Persiana Personalizada em Cidade Aracília - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você procura por uma loja que produza Persiana Personalizada em Cidade Aracília - Guarulhos para compra encontrou o lugar certo. A Maliete Decorações é uma empresa que atua com persianas, cortinas, papeis de parede, tapeçaria e decoração em geral. Atuamos com a proposta de disponibilizar produtos de alta qualidade com o preço justo que cabe no seu bolso e diversas condições de pagamento. </p>
<p>Se está procurando por Persiana Personalizada em Cidade Aracília - Guarulhos e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Maliete Decorações é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Cortina blecaute, Papel de Parede para Lavabo, Loja de Cortinas Sob Medida, Motorização de Cortinas e Cortina sob medida.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>