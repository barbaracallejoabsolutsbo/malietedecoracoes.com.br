<?php
$title       = "Persiana Motorizada em São Bernardo do Campo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Por se tratar de um produto com tecnologia integrada a Persiana Motorizada em São Bernardo do Campo possui um preço levemente mais elevado do que os demais modelos. Mas para garantir a sua tranquilidade no investimento informamos que nossos produtos são produzidos com material de componentes de alta qualidade garantindo assim uma boa usabilidade e longa vida útil para esse modelo de persiana.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Persiana Motorizada em São Bernardo do Campo? A Maliete Decorações é o que você precisa! Sendo uma das principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Cabeceira de Cama Sob Medida, Cortinas para Sala Preço, Papel de parede de linho, Cortina para quarto e Loja de Cabeceira para Cama de Solteiro. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>