<?php
$title       = "Persiana Painel no Aeroporto";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você ainda não conhece a Persiana Painel no Aeroporto, este modelo é inovador e moderno que proporciona um design diferenciado para grandes vãos. Composta por painéis com larguras que se sobrepõem para cobrir por completo quando esticadas, são perfeitas para grandes janelas e painéis de vidro. Conheça os serviços especializados em decoração de interiores da Maliete Decorações. Fale conosco e conheça mais.</p>
<p>Especialista no mercado, a Maliete Decorações é uma empresa que ganha visibilidade quando se trata de Persiana Painel no Aeroporto, já que possui mão de obra especializada em Persiana Melíade, Loja de Cabeceira para Cama de Solteiro, Persiana Motorizada para Escritório, Modelo de Cortinas para Quarto e Cabeceira para Cama de Casal. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>