<?php
$title       = "Cabeceira para Cama de Casal no Jardim Iguatemi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem diversos itens que podem decorar a cabeceira da cama, sejam em quartos de casal, crianças ou solteiros. Com a Maliete Decorações você encontra uma infinidade de possibilidades de projetos para Cabeceira para Cama de Casal no Jardim Iguatemi. Entre as opções de personalização, a Maliete oferece opções de tecidos, materiais, cores, texturas e acabamentos de acordo com sua necessidade. Consulte-nos para mais informações.</p>
<p>Se destacando entre as mais competentes empresas do segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações vem proporcionando com total empenho seja em Cabeceira para Cama de Casal no Jardim Iguatemi quanto em Cabeceira de Cama Sob Medida, Persiana Plissada, Loja de Persiana Motorizada, Cortinas de Varão e Cortina de Tecido, com o foco em agregando qualidade e excelência em seu atendimento nossa empresa busca a constante melhoria e desenvolvimento de seus colaboradores de forma a garantir o melhor para quem nos busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>