<?php
$title       = "Motorização de Cortinas na Vila Guilherme";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em ambientes comerciais ou residenciais, eliminando a necessidade de correntes e fitas, e também para quem aprecia agilidade ao sair de casa, a Motorização de Cortinas na Vila Guilherme otimiza o fechamento das persianas, permitindo que você saia ao toque de um botão. Para cortinas e persianas instaladas em janelas muito grandes ou paredes com pé direito muito alto, a motorização ajuda a abrir e fechar de forma prática.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Maliete Decorações ganha destaque quando o assunto é  Cortinas, Persianas, Papel de Parede e Tapeçarias. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Motorização de Cortinas na Vila Guilherme até Onde Comprar Canto Alemão, Cortina para Teto, Cortina de Forro e Voil, Loja de Persiana Motorizada e Persiana Romana de Teto com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>