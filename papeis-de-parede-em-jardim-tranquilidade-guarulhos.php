<?php
$title       = "Papéis de Parede em Jardim Tranquilidade - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A maior variedade de Papéis de Parede em Jardim Tranquilidade - Guarulhos você encontra aqui na Maliete Decorações. São mais de 30 anos atendendo consumidores com produtos decorativos como cortinas, persianas, tapeçarias além do papel de parede. Caso você queira saber mais ou tenha dúvidas sobre quaisquer de nossos produtos entre em contato e seja auxiliado por nossa equipe durante seu atendimento.</p>
<p>Sendo referência no ramo  Cortinas, Persianas, Papel de Parede e Tapeçarias, garante o melhor em Papéis de Parede em Jardim Tranquilidade - Guarulhos, a empresa Maliete Decorações trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Papel de parede de linho, Persiana Iluminê, Onde Comprar Cortinas Blackout, Reforma de Cortinas e Cortina de Forro e Voil para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>