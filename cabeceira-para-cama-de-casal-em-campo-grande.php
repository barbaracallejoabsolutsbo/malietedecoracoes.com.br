<?php
$title       = "Cabeceira para Cama de Casal em Campo Grande";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Entre os materiais mais comuns para cabeceira de cama estão o PVC, madeira e tapeçaria com estofado. A Maliete Decorações possui produtos e serviços especiais para você que procura por Cabeceira para Cama de Casal em Campo Grande. Conheça mais sobre nossa empresa e nossos serviços especializados para decoração de interiores que vão desde papéis de parede em diversos modelos a cortinas e persianas com designs exclusivos.</p>
<p>A Maliete Decorações, como uma empresa em constante desenvolvimento quando se trata do mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias visa trazer o melhor resultado em Cabeceira para Cama de Casal em Campo Grande para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Persiana Motorizada, Persiana Personalizada, Cortina para quarto, Cortina de linho e Persiana Rolo tela solar para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>