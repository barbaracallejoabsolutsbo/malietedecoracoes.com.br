<?php
$title       = "Persiana Double Vision em Santo André";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Persiana Double Vision em Santo André trata-se de uma persiana de enrolar, podendo ser facilmente guardada quando não necessidade de utilização,  composta por dois paineis de faixa intercalados. Outra vantagem dessa categoria de persiana é que ao abrir a janela você pode enrolar totalmente evitando movimentos bruscos causados pelo vento que podem danificar seu produto. </p>
<p>Como líder do segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações se dispõe a oferecer uma excelente assessoria nas questões de Papel de Parede Estampado, Persiana Iluminê, Motorização de Cortinas, Persiana Painel e Papéis de Parede sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Persiana Double Vision em Santo André ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>