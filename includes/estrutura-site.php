<ul>
    <li><a href="<?php echo $url; ?>index">Home</a></li>
    <li><a href="<?php echo $url; ?>#sobre-nos">Sobre nós</a></li>
    <li><a href="<?php echo $url; ?>portfolio" title="Portfólio">Portfólio</a></li>
    <li><a href="#">Produtos</a>
        <ul>
            <li><a href="<?php echo $url; ?>persianas" title="Persianas">Persianas</a></li>
            <li><a href="<?php echo $url; ?>cortina" title="Cortinas">Cortinas</a></li>
            <li><a href="<?php echo $url; ?>papel-de-parede" title="Papel de Parede">Papel de Parede</a></li>
            <li><a href="<?php echo $url; ?>tapecaria" title="Tapeçaria">Tapeçaria</a></li>
        </ul>
    </li>
    <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
    <li><a href="<?php echo $url; ?>informacoes" title="Blog">Blog</a>
        <ul>
            <?php echo $padrao->subMenu($palavras_chave); ?>
        </ul>
    </li>
</ul>