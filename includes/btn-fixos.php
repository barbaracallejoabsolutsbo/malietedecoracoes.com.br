
<div class="btn-fixo">
	<a class="btn-whatsapp" title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
       	<i class="fab fa-whatsapp"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-telephone" title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
        <i class="fas fa-phone-alt"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-instagram" title="Instagram" href="https://www.instagram.com/malietedecoracoes/">
        <i class="fab fa-instagram"></i>
   	</a>
</div>