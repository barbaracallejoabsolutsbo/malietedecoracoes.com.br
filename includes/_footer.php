<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="logo text-justify">
                    <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                        <span itemprop="image">
                            <img src="<?php echo $url; ?>imagens/logo.jpg" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h5>Contatos</h5>
                
                <p><strong>WhatsApp</strong>: <a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
                                <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a></p>
                <p><strong>Endereço</strong>: <?php echo $unidades[1]["rua"] . " <br> " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"]; ?></p> 
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <h5>Institucional</h5>
                <ul>
                    <li><a href="<?php echo $url; ?>portfolio" title="Portifólio">Portfólio</a></li>
                    <li><a href="<?php echo $url; ?>persianas" title="Persianas">Persianas</a></li>
                    <li><a href="<?php echo $url; ?>cortinas" title="Cortinas">Cortinas</a></li>
                    <li><a href="<?php echo $url; ?>papel-de-parede" title="Papel de parede">Papel de parede</a></li>
                    <li><a href="<?php echo $url; ?>tapecaria" title="Tapeçaria">Tapeçaria</a></li>
                    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                    <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                    <li><a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-left">
                    <p class="desenvolvido">Copyright © 2021 <?php echo $nome_empresa; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                    <div class="baixa-absolut"></div>
                    <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                    <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                        <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap">
                    </a>
                    <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="<?php echo $unidades[1]["whatsapp-link"]; ?>" class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
</footer>
  <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <?php } ?>