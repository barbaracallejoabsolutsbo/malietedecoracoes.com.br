<header itemscope itemtype="http://schema.org/Organization">
    <div class="container header-container-main">
        <div class="logo-info-contato">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="logo">
                        <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                            <span itemprop="image">
                                <img src="<?php echo $url; ?>imagens/logo.jpg" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1"></div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="contato-header">
                        <div class="icone">
                            <p class="btn-orcamento"><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">Faça seu Orçamento</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="menu">
                    <ul class="menu-list">
                        <?php if("http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] == $url){ ?>
                            <li><a href="javascript:void(0)" data-id="home" title="home">Home</a></li>
                            <li><a href="javascript:void(0)" data-id="sobre-nos" title="Sobre Nós">Sobre Nós</a></li>
                            <li><a href="<?php echo $url; ?>portfolio" title="Portfólio">Portfólio</a></li>
                            <li><a href="<?php echo $url; ?>#" title="Produtos">Produtos</a>
                                <ul class="sub-menu">
                                    <li><a href="<?php echo $url; ?>persianas" title="Persianas">Persianas</a></li>
                                    <li><a href="<?php echo $url; ?>cortinas" title="Cortinas">Cortinas</a></li>
                                    <li><a href="<?php echo $url; ?>papel-de-parede" title="Papel de parede">Papel de parede</a></li>
                                    <li><a href="<?php echo $url; ?>tapecaria" title="Tapeçaria">Tapeçaria</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url; ?>informacoes" title="Blog">Blog</a></li> 
                            <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                            

                        <?php }else{ ?>
                            <li><a href="<?php echo $url; ?>#home" title="home">Home</a></li>
                            <li><a href="<?php echo $url; ?>#sobre-nos" title="Sobre Nós">Sobre Nós</a></li>
                            <li><a href="<?php echo $url; ?>portfolio" title="Portfólio">Portfólio</a></li>
                            <li><a href="<?php echo $url; ?>#" title="Produtos">Produtos</a>
                                <ul class="sub-menu">
                                    <li><a href="<?php echo $url; ?>persianas" title="Persianas">Persianas</a></li>
                                    <li><a href="<?php echo $url; ?>cortinas" title="Cortinas">Cortinas</a></li>
                                    <li><a href="<?php echo $url; ?>papel-de-parede" title="Papel de parede">Papel de parede</a></li>
                                    <li><a href="<?php echo $url; ?>tapecaria" title="Tapeçaria">Tapeçaria</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url; ?>informacoes" title="Blog">Blog</a>
                            </li>
                            <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>