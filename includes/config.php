<?php

    // Principais Dados do Cliente
    $nome_empresa = "Maliete Decorações";
    $emailContato = "contato@malietedecoracoes.com.br";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "Maliete Decorações",
        "rua" => "Av. Dr. Timóteo Penteado, 4504",
        "bairro" => "Vila Galvão",
        "cidade" => "Guarulhos",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "07061-003",
        "latitude_longitude" => "-23.459076290463813, -46.564842460673745", // Consultar no maps.google.com
        "ddd" => "11",
        "whatsapp" => "94074-7048",
        "whatsapp-link" => "https://api.whatsapp.com/send?phone=5511940747048&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es",
        "link_maps" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.03671060444!2d-46.56700968554572!3d-23.45914026370929!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5baceefb885%3A0x4813cd77b3551de7!2sAv.%20Dr.%20Tim%C3%B3teo%20Penteado%2C%204504%20-%20Vila%20Galvao%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2007061-003!5e0!3m2!1spt-BR!2sbr!4v1638452106085!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
    ),

    2 => array(
        "nome" => "Maliete Decorações",
        "rua" => "Emília Marengo, 9",
        "bairro" => "Vila Gomes Cardim",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "03336-000",
        "ddd" => "11",
        "whatsapp" => "94074-7048",
        "whatsapp-link" => "https://api.whatsapp.com/send?phone=5511940747048&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es" // Incorporar link do maps.google.com
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/malietedecoracoes.com.br/",
        // URL online
    "https://www.malietedecoracoes.com.br/"
));

    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    // $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    // $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    // $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
     $smtp_contato            = "162.241.2.49";
     $email_remetente         = "dispara-email@absolutsbo.com.br";
     $senha_remetente         = "DisparaAbsolut123?";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
    $palavras_chave = array(
      "Loja de Cortinas Sob Medida",
      "Onde Comprar Cortinas Blackout",
      "Cortinas para Sala Preço",
      "Onde Comprar Papel de Parede Estampado",
      "Loja de Persiana Motorizada em São Paulo",
      "Persiana Motorizada para Escritório",
      "Cabeceira de Cama Sob Medida",
      "Loja de Papel de Parede para Banheiro",
      "Loja de Cabeceira para Cama de Solteiro",
      "Cabeceira para Cama de Casal",
      "Onde Comprar Canto Alemão em São Paulo",
      "Cabeceira para Cama King Size Preço",
      "Persiana Motorizada Preço em São Paulo",
      "Cortina de voil",
      "Cortina para sala",
      "Cortina para quarto",
      "Cortina blackout",
      "Cortina blecaute",
      "Cortina de linho",
      "Cortina sob medida",
      "Persiana para escritório",
      "Persiana para sacada",
      "Persiana para sala",
      "Persiana para quarto",
      "Persiana motorizada",
      "Persiana de madeira horizontal",
      "Persiana Rolo tela solar",
      "Persiana Blackout para quarto",
      "Persiana cinza",
      "Persiana preta",
      "Papel de parede para sala",
      "Papel de parede para o quarto",
      "Papel de parede de linho",
      "Cortinas",
      "Papéis de Parede",
      "Persiana Celular",
      "Persiana Double Vision",
      "Persiana Horizontal",
      "Persiana Iluminê",
      "Persiana Lumiére",
      "Persiana Melíade",
      "Persiana Motorizada",
      "Persiana Passione",
      "Persiana Personalizada",
      "Persiana Plissada",
      "Persiana Rolô",
      "Persiana Romana",
      "Persiana Romana de teto",
      "Persiana Vertical",
      "Manutenção de persianas",
      "Reforma de cortinas",
      "Persiana painel",
      "Persianas em alumínio",
      "Cortina para teto",
      "Cortina de tecido",
      "Cortinas romanas",
      "Cortina de forro e voil",
      "Modelo de cortinas para quarto",
      "Papel de parede estampado",
      "Papel de parede para lavabo",
      "Papel de parede infantil",
      "Motorização de cortinas",
      "Cortinas de varão",
      "Cortinas de trilho",
      "Loja de Persiana e Cortina na Zona Norte SP",
      "Loja de Persiana e Cortina na Zona Leste SP",
      "Loja de Persiana e Cortina na Zona Sul SP",
      "Loja de Persiana e Cortina na Zona Oeste SP",
      "Manutenção de Persiana em SP",
      "Loja de Persiana Rolô em SP",
      "Loja de Persiana Double Vision em SP",
      "Loja de Persiana Horizontal em SP",
      "Loja de Persiana Horizontal Aluminio em SP",
      "Loja de Persiana Horizontal Madeira em SP",
      "Loja de Persiana Horizontal Madeira Sintética em SP",
      "Loja de Persiana Vertical",
      "Manutenção de Persiana Vertical em SP",
      "Cortina de Sacada SP",
      "Persiana de Sacada SP",
      "Cortina de Varanda SP",
      "Persiana de Varanda SP",
      "Cortina de Espaço Gourmet SP",
      "Persiana de Espaço Gourmet SP",
      "Cortina Blackout SP",
      "Persiana Blackout SP",
      "Motorização de Persiana em SP",
      "Motorização de Cortina em SP",
      "Automação de Persiana em SP",
      "Automação de Cortina em SP",
      "Manutenção de Persiana Motorizada em SP",
      "Manutenção de Persiana Automatizada em SP",
      "Manutenção de Cortina Motorizada em SP",
      "Manutenção de Cortina Automatizada em SP",
      "Troca de Papel de Parede em SP",
      "Colocação de Papel de Parede em SP",
      "Loja de Cortina em SP",
      "Loja de Cortina de Tecido em SP",
      "Loja de Cortina com Forro em SP",
      "Loja de Cortina Tradicional em SP",
      "Loja de Cortina no ABC",
      "Loja de Cortina Grande SP",
      "Loja de Persiana Romana em SP"
);

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */