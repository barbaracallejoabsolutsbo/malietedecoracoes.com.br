<?php
$title       = "Persiana Rolo tela solar na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além da Persiana Rolo tela solar na Vila Carrão, na Maliete Decorações você encontra diversos outros modelos de persianas, cortinas, papeis de parede e tapeçaria em alta qualidade, não perca essa oportunidade. Caso você tenha dúvidas sobre esse ou quaisquer outros de nossos produtos entre em contato e seja atendido por um decorador especializado para te auxiliar da melhor maneira possível. </p>
<p>Buscando por uma empresa de credibilidade no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, para que, você que busca por Persiana Rolo tela solar na Vila Carrão, tenha a garantia de qualidade e idoneidade, contar com a Maliete Decorações é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Persiana Motorizada para Escritório, Cortina para quarto, Cabeceira para Cama de Casal, Cortina de Forro e Voil e Manutenção de Persianas para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>