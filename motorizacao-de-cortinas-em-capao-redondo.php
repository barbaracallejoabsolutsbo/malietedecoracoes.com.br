<?php
$title       = "Motorização de Cortinas em Capão Redondo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em ambientes comerciais ou residenciais, eliminando a necessidade de correntes e fitas, e também para quem aprecia agilidade ao sair de casa, a Motorização de Cortinas em Capão Redondo otimiza o fechamento das persianas, permitindo que você saia ao toque de um botão. Para cortinas e persianas instaladas em janelas muito grandes ou paredes com pé direito muito alto, a motorização ajuda a abrir e fechar de forma prática.</p>
<p>Sendo uma das principais empresas do segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações possui os melhores recursos do mercado com o objetivo de disponibilizar Persiana Celular, Loja de Cabeceira para Cama de Solteiro, Persiana Romana, Cortina para Teto e Cabeceira para Cama de Casal com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Motorização de Cortinas em Capão Redondo com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>