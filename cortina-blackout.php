<?php
    $title       = "Cortina blackout";
    $description = "A cortina blackout proporciona uma atmosfera mais aconchegante e um espaço mais escuro para a hora de dormir ou de assistir televisão.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Maliete está há mais de 30 anos no mercado e tem como objetivo trazer a melhor qualidade não só na <strong>cortina blackout,</strong> mas em todos os tipos de cortinas, persianas e papeis de parede do nosso portfólio, transformando seu lar. Nosso foco é proporcionar aos nossos clientes os melhores produtos de decoração interior, deixando seu espaço mais bonito e confortável.</p>
<p>Todos os nossos produtos, desde a <strong>cortina blackout</strong> até papeis de parede e persianas são feitos sob medida. Tudo é pensado para que sua sala, quarto, escritório, ou qualquer outro ambiente, seja um local com seu estilo e personalidade.</p>
<p>Estamos localizados em São Paulo, atualmente em dois locais de atendimento – Av. Timóteo Penteado, 4504 ou Rua Emília Marengo, 09 - e para melhor recebe-los estamos trabalhando com hora marcada. Nossos profissionais possuem experiência e conhecimento não só a respeito da <strong>cortina blackout,</strong> mas de todos os produtos de nosso catálogo.</p>
<p>Quando falamos de decoração, as janelas devem ter um cuidado específico, já que seu manuseio exerce grandes mudanças e impacto nos interiores. Cada espaço possui suas necessidades e funcionalidades, portanto, combinar a beleza estética é fundamental para torna-lo mais agradável, e assim, mais funcional.</p>
<p>A Maliete Decorações se preocupa não só com a beleza de seus espaços, mas também com seu bem-estar. Por isso, falar de decoração é muito mais do que apresentar objetos bonitos para um local agradável. Nosso trabalho é viabilizar os melhores produtos capazes de exercer sua melhor função, tudo isso com beleza, responsabilidade e profissionalismo.</p>
<h2>Conheça a <strong>cortina blackout</strong></h2>
<p>As cortinas, além de decorativas, são objetos diretamente relacionas com a iluminação do ambiente. A <strong>cortina blackout</strong> proporciona uma atmosfera mais aconchegante e um espaço mais escuro para a hora de dormir ou de assistir televisão. Seja em salas, quartos ou escritórios, a <strong>cortina blackout</strong>, também conhecida como cortina corta luz, irá bloquear a entrada de luminosidade.</p>
<p>Com excelente custo benefício, <strong>a cortina blackout</strong> leva privacidade para seus interiores. Esse modelo de cortina é feito com diversos tipos de tecido, e sua instalação deve acompanhar toda a área de extensão da janela para cumprir sua função. Algumas dúvidas surgem quanto às cores utilizadas. Apesar do nome “blackout”, esse tipo de cortina não é feita exclusivamente com tecidos escuros.</p>
<p>A <strong>cortina blackout</strong> pode ser preta, branca, amarela, vermelha, caramelo, marrom, cinza... a cor que você desejar. O que impede a passagem da luz e garante sua funcionalidade é a espessura do tecido e o material utilizado. Seu modelo pode ser desde painel, tecido, romana, e o material inclui tecido de algodão, poliéster, PVC e assim por diante.</p>
<p>Quanto à limpeza da <strong>cortina blackout, </strong>vai depender do tipo de material escolhido. Quando de algodão ou poliéster podem ser lavadas em máquinas e secadas com cuidado, já que altas temperaturas podem encolher o tecido.</p>
<p>Já a <strong>cortina blackou</strong>t de PVC, por exemplo, pode ser limpa de forma ainda mais prática, com esponja ou um pano molhado, assim como a <strong>cortina blackout</strong> em estilo persiana, a qual pode ser higienizada no próprio local em que está fixada.</p>
<h3>Vantagens na escolha de uma <strong>cortina blackout</strong></h3>
<p>Para quem gosta de um modelo tradicional e elegante, a combinação da <strong>cortina blackout</strong> com voil é uma boa aposta. Além do bloqueio da luz, sua estética é sofisticada e harmoniosa. Dependendo da escolha de estampas e cores, diferentes movimentos e sensações serão transmitidas para o ambiente, sendo sua versatilidade uma de suas melhores vantagens.</p>
<p>Não apenas em quartos, a <strong>cortina blackout</strong> vem sendo utilizada em ambientes compartilhados e de convivência como salas de estar ou salas de jantar. Muitas casas e apartamentos com ambientes destinados a aparelhos de televisão, por exemplo, pedem por uma <strong>cortina blackout</strong> para melhor aproveitamento do local. Quando utilizadas na sala de estar, especificamente, é interessante um tecido claro e neutro para que seu interior pareça mais amplo e leve.</p>
<p>Algumas vantagens da <strong>cortina blackout</strong> são:</p>
<ul>
<li>         Proteção contra o sol</li>
<li>         Controle de luminosidade</li>
<li>         Versatilidade</li>
<li>         Privacidade</li>
<li>         Conforto térmico e visual</li>
</ul>
<p>Conhecendo melhor as vantagens de adquirir uma <strong>cortina blackout</strong>, saiba que nossa equipe trabalha com materiais de primeira linha independente do projeto que está sendo feito. Tudo é pensado para que você tenha a melhor experiência e o melhor resultado decorativo, com um ambiente que seja sua cara.</p>
<p>Decoração e identidade estão interligadas. Nosso estilo também reflete em nossas escolhas decorativas, colocando personalidade nos ambientes em que estamos. Por isso, quando falamos de roupas, acessórios, móveis, utensílios domésticos... e até decoração interior, sua opinião não pode ficar de fora. Escolher o melhor tipo de <strong>cortina blackout</strong> para você para sua família é uma dica especial de nossa loja, e estamos aqui para ajuda-lo.</p>
<p>A decoração de interiores vem para promover diferentes sensações e consagrar o melhor aproveitamento do espaço. A <strong>cortina blackout</strong> traz aconchego, privacidade e proporciona melhores ambientes para o tipo de atividade que você deseja, seja para lazer – como assistir um filme em clima de cinema – como para criar uma melhor atmosfera na hora de dormir, e assim por diante.</p>
<p>Nossa equipe esta sempre estudando e se adaptando as mudanças dessa área, para que você, nosso cliente, receba o máximo de conforto e qualidade em nossos produtos. Trabalhamos para que os preços de nossos produtos sejam acessíveis e compatíveis com seu bolso, para que você possa adquiri-los no momento em que desejar e precisar.  </p>
<p>Consideramos nossos clientes parte de nossa história, por isso, cada projeto é único e especial para nós. Priorizamos a qualidade e o atendimento de nossos serviços para que nossos clientes tenham o resultado que desejam e seu espaço bem decorado.</p>
<p>Assim, para que você tenha a melhor experiência de compra, entre em contato com nossa loja e conheça um de nossos profissionais. Ele estará disponível para que todas as suas dúvidas sejam tiradas sobre a nossa <strong>cortina blackout, </strong>ou qualquer outro produto. Nós da Maliete Decorações garantimos o melhor atendimento que você procura, a qualquer momento.</p>
<p>Faça o seu orçamento através do nosso Whatsapp e qualquer dúvida não hesite! Estamos prontos para melhor atende-los.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>