<?php
$title       = "Papel de Parede Estampado em Tanque Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Feitos em alta qualidade de impressão, em materiais diversos como PVC, de alta resistência, nosso Papel de Parede Estampado em Tanque Grande - Guarulhos pode ser encontrado na Maliete Decorações em diversos modelos, cores e texturas. Conheça nossos produtos para decoração de interiores que vão desde papel de parede até persianas e cortinas. Especiais para decoração de quartos, salas, escritórios, entre outros ambientes.</p>
<p>Pensando em proporcionar a todos os clientes o melhor em métodos quando se trata de  Cortinas, Persianas, Papel de Parede e Tapeçarias? A empresa Maliete Decorações vem ganhando destaque em referência no assunto de Papel de Parede Estampado em Tanque Grande - Guarulhos. Por isso, solicite um orçamento para Persiana Painel, Persiana Melíade, Persiana Motorizada para Escritório, Cortinas de Trilho e Cortina sob medida assim você contará com a qualidade que somente a nossa empresa oferece para todos os seus clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>