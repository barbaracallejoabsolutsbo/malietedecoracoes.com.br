<?php
    $title       = "Cortina de Varanda SP";
    $description = "Nós da Maliete Decorações, somos uma empresa de cortina de varanda SP que possui os melhores preços e opções para atender da melhor forma nossos clientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/".$url_title."/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/".$url_title."/".$url_title."-thumb.jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <h2>Você esta pesquisando por uma cortina de varanda SP?</h2>
<p><br />Nossa empresa é a melhor quando falamos em cortina de varanda SP!</p>
<p>Buscando sempre trazer as melhores novidades do segmento, temos profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos!</p>
<p>Na maioria das vezes quando vamos decorar um imóvel, pensamos que não tem necessidade em colocar uma cortina de varanda SP, já que a cortina seria apenas por decoração. Essa forma de pensar muda após a casa ou escritório estar totalmente decorado, pois enxergamos a necessidade da cortina de varanda SP por diversos motivos.</p>
<p>Com a escolha do melhor produto para cada espaço, a luz torna-se uma valiosa ferramenta decorativa e contribui significativamente para a criação de um ambiente prazeroso e aconchegante.</p>
<p>As cortinas rolô tela solar é ideal para sacadas. Ela combina com qualquer decoração e qualquer projeto, funcionando como uma tela, um painel vertical que pode ser esticado, deixado a meia altura ou totalmente recolhido e enrolado.</p>
<p>Nos ambientes onde tem janelas e sacadas com vista para os vizinhos, é necessário uma cortina para manter a privacidade. Além disso, a cortina de varanda SP também ajuda a economizar energia, já que permite que a gente possa regular a luminosidade desejada no ambiente e baixando a temperatura quando o sol bate durante o dia, evitando ligar a luz ou aparelhos como ventiladores e ar condicionado.</p>
<p>A cortina de varanda SP traz diversos benefícios para sua sacada, como o bloqueio dos perigosos raios UV, privacidade, proteção para você e para sua família (de bichos e mosquitos), abertura na medida desejada, deixar o ambiente e o ar mais limpo por não deixar o ar de fora vim direto para o imóvel, limpeza de forma mais rápida e prática, entre outros benefícios.</p>
<p>Essas cortinas possuem três fatores de abertura da trama:</p>
<ul>
<li>Coleção 1% - tem a trama mais fechada, tendo pouca visibilidade para área externa;</li>
<li>Coleção 3% - tem a trama intermediária, com visibilidade parcial;</li>
<li>Coleção 5% - tem a trama do tecido mais aberta, proporcionando mais visibilidade para o exterior;</li>
<li>Cortina de varanda SP é com a gente.</li>
</ul>
<p>Entre em contato pelo telefone ou e-mail e faça um orçamento de cortina de varanda SP com a Maliete Soluções.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>