<?php
$title       = "Cortina de linho em Capelinha - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa principal proposta é oferecer produtos de qualidade com preço justo para você decorar ou redecorar seu ambiente sempre que precisar. Caso você queira saber mais sobre a Cortina de linho em Capelinha - Guarulhos ou tenha dúvidas sobre esse, ou quaisquer outros de nossos produtos entre em contato e seja atendido por um decorador especializado de nossa equipe para te auxiliar. </p>
<p>Tendo como especialidade Loja de Papel de Parede para Banheiro, Papel de parede para sala, Papel de parede de linho, Cortina de Tecido e Persianas em Alumínio, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias. Por isso, quando falamos de Cortina de linho em Capelinha - Guarulhos, buscar pelos membros da empresa Maliete Decorações é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>