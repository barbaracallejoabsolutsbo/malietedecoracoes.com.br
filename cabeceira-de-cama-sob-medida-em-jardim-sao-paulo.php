<?php
$title       = "Cabeceira de Cama Sob Medida em Jardim São Paulo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Trabalhamos com profissionais com mais de 20 anos de experiência no ramo de tapeçaria, profissionais estes que garantem alta qualidade em canto alemão e Cabeceira de Cama Sob Medida em Jardim São Paulo. Contrate a Maliete Decorações para projetos exclusivos de canto alemão, cabeceiras, cortinas, persianas e papéis de parede para decoração dos mais diversos ambientes. Acesse nosso site e veja nosso portfólio.</p>
<p>Nós da Maliete Decorações trabalhamos dia a dia para garantir o melhor em Cabeceira de Cama Sob Medida em Jardim São Paulo e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Cortina para Teto, Cortinas para Sala Preço, Cortinas, Persiana Horizontal e Loja de Papel de Parede para Banheiro e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>