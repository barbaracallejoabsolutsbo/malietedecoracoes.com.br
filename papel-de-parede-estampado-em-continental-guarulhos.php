<?php
$title       = "Papel de Parede Estampado em Continental - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Feitos em alta qualidade de impressão, em materiais diversos como PVC, de alta resistência, nosso Papel de Parede Estampado em Continental - Guarulhos pode ser encontrado na Maliete Decorações em diversos modelos, cores e texturas. Conheça nossos produtos para decoração de interiores que vão desde papel de parede até persianas e cortinas. Especiais para decoração de quartos, salas, escritórios, entre outros ambientes.</p>
<p>Se você está em busca por Papel de Parede Estampado em Continental - Guarulhos conheça a empresa Maliete Decorações, pois somos especializados em  Cortinas, Persianas, Papel de Parede e Tapeçarias e trabalhamos com variadas opções de produtos e/ou serviços para oferecer, como Cortina de Tecido, Persiana Rolo tela solar, Persiana Passione, Persiana Rolô e Loja de Papel de Parede para Banheiro. Sabendo disso, entre em contato conosco e faça uma cotação, temos competentes profissionais para dar o melhor atendimento possível para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>