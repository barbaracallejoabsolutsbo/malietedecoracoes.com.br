<?php
    $title       = "Cortina sob medida";
    $description = "A  cortina sob medida é pensada para que seu ambiente seja um local com estilo e com sua personalidade.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Quando você pensa na decoração que deseja criar em sua casa, é difícil encontrar exatamente os itens de decoração que almejados. Isso pode acontecer com objeto por muitos considerado simples, como as cortinas.</p>
<p>As janelas são parte importante e merecem a nossa atenção quando o assunto é decoração. Seu manuseio exerce grandes mudanças e impactos nos interiores; e como cada espaço possui sua funcionalidade, o mais indicado é combinar estética e função para tornar o ambiente mais agradável.</p>
<p>O que você quer, você encontra na Maliete. Todos os nossos produtos são entregues com qualidade diferenciada e garantia de um ótimo atendimento.</p>
<h2>Conheça nossa loja</h2>
<p>Temos mais de 30 anos no mercado e nosso objetivo é levar a melhor qualidade para você. Isso inclui objetos específicos como a <strong>cortina sob medida. </strong>Queremos transformar seu lar e nosso foco é garantir os melhores produtos de decoração interior, deixando seu espaço bonito e confortável.</p>
<p>A <strong>cortina sob medida</strong> é pensada para que seu ambiente seja um local com estilo e com sua personalidade. Sendo esse objeto responsável pela privacidade e luminosidade, escolhe-la com cautela irá trazer exatamente o conforto que você deseja.</p>
<p>Atualmente possuímos dois locais de atendimento em São Paulo: Av. Timóteo Penteado, 4504 e Rua Emília Marengo, 09. Para melhor atendimento estamos trabalhando com hora marcada, e nossos profissionais possuem não só experiência, como conhecimento de todos os nossos produtos, incluindo a <strong>cortina sob medida.</strong></p>
<p>A Maliete se preocupa com a beleza de seu espaço e também com seu bem-estar. Por isso, quando falamos de decoração, não apresentamos apenas os objetos mais bonitos para um local agradável, mas também, temos como valor, viabilizar os melhores produtos para que exerçam suas melhores funções. Tudo isso com muita responsabilidade e profissionalismo.</p>
<h3>Porque escolher a cortina dos seus sonhos?</h3>
<p>Escolher uma <strong>cortina sob medida</strong> pode parecer para alguns um capricho ou vaidade, mas o que devemos lembrar é que as cortinas têm utilidades variadas, podendo compor parte da estética, cumprir uma função específica, ou ambos.</p>
<p>Na maioria dos casos, a cortina, seja ela a <strong>cortina sob medida</strong> ou não, recebe destaque por ser um elemento bastante visível dentro de um ambiente. Por isso, quando bem ajustada às janelas resultam em perfeição para aqueles que a notarem.</p>
<p>Seja qual for o local dentro de sua casa ou apartamento, um dos itens que mais podem contribuir para a decoração é a <strong>cortina sob medida, </strong>por que a iluminação importa para qualquer tipo de ambiente.</p>
<p>De acordo com o projeto, a <strong>cortina sob medid</strong>a irá ostentar formas, cores e texturas, assim, sua customização confere caráter único e diferenciado para seu ambiente. Mais que estética, a <strong>cortina sob medida</strong> oferece proteção. Na compra da <strong>cortina sob medida</strong>, toda a extensão da janela será conquistada, protegendo o local de sol, vento, e evitando luminosidade indesejada.</p>
<p>A <strong>cortina sob medida</strong> vai proporcionar uma atmosfera mais discreta ou chamativa, simples ou sofisticada, dependendo do seu gosto e desejo, possibilitando um espaço mais equilibrado, charmoso e, principalmente, mais confortável para você.</p>
<p>O tecido da<strong> cortina sob medida</strong> é uma escolha pessoal, assim como sua cor. Essa categoria permite qualidade em todos os quesitos, afinal é você que escolherá o tecido, acabamento, acessórios, cores, e tudo o que tem direito. Por exemplo, cores mais neutras e claras aplicam suavidade no ambiente. Para um local mais requintado, a <strong>cortina sob medida</strong> com tecidos nobres possuem bom caimento. Se você possui um estilo mais contemporâneo e moderno, outros tecidos, cores e tons da <strong>cortina sob</strong> <strong>medida</strong> podem fazer toda a diferença.</p>
<p>A escolha depende do tipo de ambiente que você deseja criar, ou seja, bordada, lisa, vazada ou estampada, a <strong>cortina sob medida </strong>deve ser combinada com o restante do interior; podendo ser confeccionada de variados materiais e tipos tecido, a escolha da <strong>cortina sob medida</strong> irá influenciar na duração do produto e nas sensações que serão transmitidas.</p>
<p>Além do aspecto estético, <strong>a cortina sob medida</strong> deve ter uma escolha minuciosa do material, para que seja prática e possa ser facilmente limpada, assim como devidamente secada. A escolha da <strong>cortina sob medida</strong> permite a proteção dos móveis contra o excesso de claridade, além de impedir reflexos em objetos como televisões.</p>
<p>Por exemplo, a cortina de linho transforma a atmosfera com um toque acolhedor e de simplicidade. Já a cortina de voil, com material leve e transparente, comum para esse tipo de cortina, transmite suavidade e charme para o ambiente.</p>
<p>O melhor aspecto da <strong>cortina sob medida</strong> é que colocamos nossa personalidade e desejo diretamente no ambiente. Estando a decoração diretamente ligada a manifestação de nossos gostos e preferencias, é importante ter em mente que escolhe uma <strong>cortina sob medida </strong>é uma grande forma de tornar seu espaço mais “seu”.</p>
<p>Conhecendo as vantagens de uma <strong>cortina sob medida</strong>, saiba que nossa equipe trabalha com materiais de primeira linha independente do projeto que está sendo executado. Tudo é pensado para que você tenha a melhor experiência e o melhor resultado decorativo, com ambientes que sejam do jeito que você imaginou.</p>
<p>A decoração de interiores tem como um de seus principais focos, promover diferentes sensações e consagrar o melhor aproveitamento do espaço. O resultado desse trabalho é despertar aconchego, produtividade e proporcionar melhores ambientes para lazer, negócios, convivência, e assim por diante.</p>
<p>Nossa equipe esta focada e dedicada em estudar e adaptar cada atendimento para acompanhamento das mudanças dessa área, para que você, nosso cliente, receba o máximo de conforto e qualidade com nosso serviço. Trabalhamos para que os preços de nossos produtos sejam acessíveis e compatíveis com seu bolso, para que você possa adquiri-los no momento em que precisar e desejar.</p>
<p>Cada projeto é único e especial para nós, pois consideramos nossos clientes parte de nossa história. Sendo assim, a qualidade e o atendimento de nossos serviços são nossas prioridades para que todos os nossos clientes tenham o melhor resultado que desejam, além de um espaço bem decorado.</p>
<p>Faça o seu orçamento através do nosso Whatsapp e qualquer dúvida não hesite! Estamos prontos para melhor atende-los.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>