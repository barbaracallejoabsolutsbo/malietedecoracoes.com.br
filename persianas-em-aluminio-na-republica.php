<?php
$title       = "Persianas em Alumínio na República";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Persianas em Alumínio na República são muito duráveis, higiênicas e de fácil manutenção. Feitas em material resistente e que bloqueia os raios solares, proporciona um ambiente com iluminação controlada e perfeitas para privacidade, modernidade e conforto. Com a Maliete Decorações você encontra persianas em modelos de alumínio, pvc, entre outros materiais, tecidos, cores e texturas. Conheça mais sobre nossos produtos.</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é uma empresa experiente quando se trata do ramo de Persiana Motorizada Preço, Cortina de voil, Cabeceira de Cama Sob Medida, Persiana Celular e Persiana Rolô. Por isso, se você busca o melhor com um custo acessível e vantajoso em Persianas em Alumínio na República aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>