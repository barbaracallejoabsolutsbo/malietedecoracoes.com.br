<?php
$title       = "Onde Comprar Canto Alemão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Maliete Decorações é o melhor local para você que procura pôr onde comprar canto alemão sob medida. Com uma ala especializada em tapeçaria, oferecemos produtos de alta qualidade e matéria prima de padrão inquestionável. Nossos profissionais, com mais de duas décadas de experiência, garantem excelência em acabamento e design. Faça seu projeto conosco agora mesmo e consulte-nos para mais informações.</p><h2>Onde comprar canto alemão em Guarulhos</h2><p>Em Guarulhos, para você que busca um lugar onde comprar canto alemão é fácil e com preço justo, conheça a Maliete Decorações. Localizados no bairro da Vila Galvão, oferecemos o que há de melhor em tapeçaria para confecção de cabeceiras para camas. Com design luxuoso, acabamento impecável e alto padrão de qualidade, compre com a Maliete Decorações e tenha suporte do início ao fim do projeto.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>