<?php
$title       = "Persiana para sala na Vila Formosa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa loja está há mais de 30 anos no mercado atendendo clientes com opções incríveis de cortinas, persianas, papéis de parede e tapeçaria de alta qualidade. Buscamos oferecer produtos com boa qualidade e preço justo para continuar como referência dentro desse segmento no Brasil. Não perca essa oportunidade e solicite agora meu sua Persiana para sala na Vila Formosa com as melhores condições de pagamento. </p>
<p>Como uma empresa especializada em  Cortinas, Persianas, Papel de Parede e Tapeçarias proporcionamos sempre o melhor quando falamos de Papel de parede para sala, Cortinas para Sala Preço, Cortina para quarto, Cortina sob medida e Persiana Double Vision. Com potencial necessário para garantir qualidade e excelência em Persiana para sala na Vila Formosa com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Maliete Decorações trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>