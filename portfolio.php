<?php

    $title       = "Portfólio";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "home",
        "portifolio"

    )); ?>


</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="topo-pags">
            <div class="container">
                <h1><?php echo $h1;?></h1>
                <hr>
            </div>
        </div>
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
           
            <div class="contador-home">
                <div class="inf-portfolio">
                    <div class="container">
                        <h3>PRODUTOS MALIETE</h3>
                        <p>Produtos: Cortinas; Papéis de Parede; Persiana Celular; Persiana Double Vision; Persiana Horizontal; Persiana Iluminê; Persiana Lumiére; Persiana Melíade; Persiana Motorizada; Persiana Passione; Persiana Personalizada; Persiana Plissada; Persiana Rolô; Persiana Romana; Persiana Romana de teto; Persiana Vertical.</p>
                        <p>Todos os produtos na Maliete são sob medida.</p>
                        <h4>Faça seu orçamento através do nosso Whatsapp.</h4>
                    </div>        
                </div>
                <div class="container">
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-20.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-20.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Rolô em bege</h3>
                                        <p>Persiana Rolô screen 3% em bege decorando um belo escritório.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-19.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-19.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Voi Leda</h3>
                                        <p>Cortina no modelo wave com voil leda e barra simples.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-18.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-18.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Voil Leda</h3>
                                        <p>Cortina de 5,15m de comprimento feita de voil Leda, com barra dupla e acabamento wave.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-17.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-17.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de voil</h3>
                                        <p>Cortina de voil Leda com 3x de franzido no tecido.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-16.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-16.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de voil liso com forro</h3>
                                        <p>Cortina de voil liso branco com forro em microfibra cinza.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-15.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-15.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Horinzontal de madeira</h3>
                                        <p>Persiana Horizontal de Madeira.<br>fonte: Pinterest</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-14.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-14.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Painel</h3>
                                        <p>Persiana Painel em cinza.<br>foto: Pinterest</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-13.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-13.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Vertical</h3>
                                        <p>Persiana Vertical em bege.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-12.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-12.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Voil</h3>
                                        <p>Cortina de Voil liso.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-11.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-11.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede linha Roll in Stones</h3>
                                        <p>Papel de parede linha Roll in Stones (bloquinhos).<br>fonte: Pinterest</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-10.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-10.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Rolô</h3>
                                        <p>Persiana Rolô em preto com galeria branca.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-9.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-9.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Romana com chale</h3>
                                        <p>Persiana Romana em branco com chale marrom.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-8.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-8.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Double Vision em preto</h3>
                                        <p>Persiana Double Vision em preto com galeria branco.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-7.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-7.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede linha Livina</h3>
                                        <p>Papel de parede linha Livina com ranhuras de folhas.<br>fonte: Pinterest</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-6.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-6.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Quarto em seda</h3>
                                        <p>Cortina com chale em seda, prega fêmea na sanca e com franzimento em 3x de tecido.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-5.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-5.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de voil com chale no quarto</h3>
                                        <p>Cortina de voil Judith no quarto com chale listrado.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-4.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-4.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Sala voil liso e chale</h3>
                                        <p>Cortina de  sala em voil liso branco e chale em chantum embutidos com trilho suiço na sanca.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-3.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-3.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Romana</h3>
                                        <p>Persiana Romana branca.<br>foto: Pinterest</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-2.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-2.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Linho</h3>
                                        <p>Cortina de Gaze Linho feita para um sacada.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-1.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-1.jpg" class="img-responsive content-image" alt="Cortina de Linho" title="Cortina de Linho">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de Linho</h3>
                                        <p>Cortina de Linho com um alto pé direito e 3x de franzido.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-29.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-29.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina blackout</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-30.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-30.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina blecaute</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-31.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-31.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de forro e voil</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-32.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-32.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de linho</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-33.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-33.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina de tecido</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-34.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-34.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana para sacada</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-35.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-35.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana para quarto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-36.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-36.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana para sala</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-37.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-37.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina para teto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-38.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-38.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortinas</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-39.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-39.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortinas de trilho</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-40.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-40.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortinas de varão</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-41.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-41.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Blackout para quarto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-42.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-42.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortinas romanas</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-43.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-43.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Manutenção de persianas</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-44.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-44.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Modelo de cortinas para quarto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-45.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-45.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Motorização de cortinas</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-46.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-46.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papéis de Parede</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-47.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-47.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede de linho</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-48.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-48.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede estampado</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-49.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-49.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede infantil</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-50.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-50.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede para lavabo</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-51.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-51.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede para o quarto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-52.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-52.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Papel de parede para sala</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-53.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-53.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Blecaute</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-54.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-54.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Celular</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-55.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-55.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Cortina para sala</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-56.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-56.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Double Vision</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-57.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-57.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Horizontal</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-58.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-58.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Lumiére</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-59.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-59.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Melíade</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-60.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-60.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Motorizada</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-61.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-61.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana painel</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-62.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-62.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana para escritório</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-63.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-63.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Passione</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-64.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-64.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Personalizada</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-65.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-65.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Plissada</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-66.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-66.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana preta</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-67.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-67.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Rolô</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-68.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-68.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Romana</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-69.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-69.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Romana de teto</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-70.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-70.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persianas em alumínio</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-71.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-71.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Persiana Vertical</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="lista-galeria-fancy row">
                        <div class="desce-pag col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="content">
                                <a href="<?php echo $url; ?>imagens/produtos/produtos-72.jpg">
                                    <div class="content-overlay"></div>
                                    <img src="<?php echo $url; ?>imagens/produtos/produtos-72.jpg" class="img-responsive content-image" alt="imagem" title="imagem">
                                    <div class="content-details fadeIn-top">
                                        <h3>Reforma de cortinas</h3>
                                        <p>.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                </div> 
            </div> 
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/jquery.nivo",

    )); ?>

</body>
</html>