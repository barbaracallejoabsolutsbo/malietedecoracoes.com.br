<?php
    $title       = "Automação de Cortina em SP";
    $description = "A Maliete Soluções é uma empresa de automação de cortina em SP, tendo como objetivo oferecer excelentes produtos com rapidez na entrega e instalação.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url; ?>imagens/imagens/thumbs/thumb.jpg" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url; ?>imagens/thumbs/thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <h2>Você esta precisando de automação de cortina em SP?</h2>
<p>Automação de cortina em SP é com a Maliete Soluções!</p>
<p>Nossa empresa é especialista no segmento residencial e corporativo, buscando atender as mais variadas exigências dos projetos residenciais e comerciais, procurando sempre trazer a qualidade e eficiência para todos os ambientes.</p>
<p>Quando falamos em automação de cortina em SP, precisamos saber a diferença entre persiana e cortina, já que embora tenham praticamente a mesma função, se diferenciam em muitos aspectos. As persianas são feitas com alumínio, plástico, madeira ou qualquer material de origem rústica. Já as cortinas são feitas a partir de tecidos com uma forma muito mais artesanal, podendo ser combinados com itens decorativos e tendo um caimento completo.</p>
<p>A automação de cortina em SP deixou de ser uma idéia futurista, já que hoje temos tecnologias mais acessíveis permitindo modernizar nosso estilo de vida, virando tendência em design e arquitetura.</p>
<p>A automação de cortina em SP ajudam a decorar o espaço, contribui com os cenários de iluminação, fornecem proteção solar e ajudam a manter a privacidade das pessoas que estão presentes naquele local, tendo um papel importante na residência.</p>
<p>A automação de cortina em SP aumenta a vida útil do produto além de proporcionar maior conforto para os usuários, sendo fundamental para produtos grandes e ideais para qualquer ambiente.</p>
<p>Os acionamentos da automação de cortina em SP podem ser feitos por controle remoto ou interruptor, além de interagir com centrais de automação do cliente.</p>
<p>O trilho motorizado é uma opção para o deslocamento lateral de cortinas de tecido e para cortinas do tipo painel. O acionamento desse trilho pode ser feito puxando suavemente a cortina, utilizando controle remoto ou com algum emissor.</p>
<p>Existe também a automação de cortina em SP por conectores que são ligados a um sistema de automação, que permite o acionamento de cortinas, persianas e toldos, tudo pela internet via tablet ou celular.</p>
<p>Independente das opções escolhida para a sua residência, a motorização da persiana vai trazer muita comodidade e funcionalidade para você e todas as pessoas que freqüentam aquele lugar.</p>
<p>Entre em contato com a gente e faça um orçamento de automação de cortina em SP!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>