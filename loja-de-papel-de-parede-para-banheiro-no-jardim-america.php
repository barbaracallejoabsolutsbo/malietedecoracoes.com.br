<?php
$title       = "Loja de Papel de Parede para Banheiro no Jardim América";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Localizada na cidade de Guarulhos, no bairro da Vila Galvão, a Maliete Decorações é a maior Loja de Papel de Parede para Banheiro no Jardim América da região. Visite-nos e traga sua ideia de cortinas, persianas e papéis de parede e tenha suporte profissional para desenvolver seu projeto do início ao fim. Papéis de parede para lavabo, para quarto, sala, escritório e muito mais. Encontre também suporte para motorizar persianas e cortinas.</p>
<p>Especialista no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Loja de Papel de Parede para Banheiro no Jardim América. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Persiana Blackout para quarto, Persiana Personalizada, Manutenção de Persianas, Loja de Persiana Motorizada e Cortinas para Sala Preço e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>