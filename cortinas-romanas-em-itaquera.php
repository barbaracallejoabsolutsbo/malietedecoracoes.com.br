<?php
$title       = "Cortinas Romanas em Itaquera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os melhores locais para instalação de Cortinas Romanas em Itaquera são salas de estar, quartos, escritórios, salas de tv, entre outros. Além de combinar mais com os ambientes citados, a cortina romana pode ser feita em diferentes tecidos, cores e texturas que oferecem design exclusivo e de acordo com sua necessidade. Consulte nosso atendimento para mais informações e encontre cortinas especiais para você.</p>
<p>Desempenhando uma das melhores assessorias do segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Cortinas Romanas em Itaquera com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Persiana Rolo tela solar, Loja de Persiana Motorizada, Papel de parede de linho, Persiana Romana e Persiana Personalizada, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>