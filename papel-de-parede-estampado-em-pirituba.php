<?php
$title       = "Papel de Parede Estampado em Pirituba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Feitos em alta qualidade de impressão, em materiais diversos como PVC, de alta resistência, nosso Papel de Parede Estampado em Pirituba pode ser encontrado na Maliete Decorações em diversos modelos, cores e texturas. Conheça nossos produtos para decoração de interiores que vão desde papel de parede até persianas e cortinas. Especiais para decoração de quartos, salas, escritórios, entre outros ambientes.</p>
<p>A empresa Maliete Decorações é destaque entre as principais empresas do ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Papel de Parede Estampado em Pirituba do mercado. Ainda, possui facilidade com Persiana Vertical, Cortinas para Sala Preço, Persiana Iluminê, Persianas em Alumínio e Cortina de Forro e Voil mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>