<?php
$title       = "Persiana Motorizada para Escritório em Morros - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Com mão de obra especializada e experiente para instalação de sua Persiana Motorizada para Escritório em Morros - Guarulhos, oferecemos preço acessível e alta qualidade em papéis de parede, cortinas e persianas. A Maliete Decorações, localizada em Guarulhos, trabalha com matéria prima de alto padrão e artigos com design exclusivo para decoração do seu lar ou escritório. Conheça mais acessando o site.</p>
<p>Nós da Maliete Decorações trabalhamos dia a dia para garantir o melhor em Persiana Motorizada para Escritório em Morros - Guarulhos e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Cortina de Tecido, Persiana Personalizada, Cabeceira para Cama de Casal, Persiana Blackout para quarto e Cortina sob medida e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>