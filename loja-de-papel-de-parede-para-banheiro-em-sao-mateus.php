<?php
$title       = "Loja de Papel de Parede para Banheiro em São Mateus";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Localizada na cidade de Guarulhos, no bairro da Vila Galvão, a Maliete Decorações é a maior Loja de Papel de Parede para Banheiro em São Mateus da região. Visite-nos e traga sua ideia de cortinas, persianas e papéis de parede e tenha suporte profissional para desenvolver seu projeto do início ao fim. Papéis de parede para lavabo, para quarto, sala, escritório e muito mais. Encontre também suporte para motorizar persianas e cortinas.</p>
<p>Com a Maliete Decorações proporcionando o que se tem de melhor e mais moderno no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Cortinas para Sala Preço, Cabeceira de Cama Sob Medida, Persiana Melíade, Papéis de Parede e Persiana Blackout para quarto nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Loja de Papel de Parede para Banheiro em São Mateus.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>