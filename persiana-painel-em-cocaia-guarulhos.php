<?php
$title       = "Persiana Painel em Cocaia - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Maliete Decorações está no mercado há mais de 30 anos e oferece os melhores serviços para decoração de interiores. Para persianas em geral, cortinas e papéis de parede, encontre serviços especializados em instalação, manutenção e reforma com mão de obra especializada. Compre com o melhor custo benefício do mercado regional, Persiana Painel em Cocaia - Guarulhos com design moderno e luxuoso. Fale conosco.</p>
<p>Com a Maliete Decorações proporcionando o que se tem de melhor e mais moderno no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Persiana Horizontal, Persiana Personalizada, Onde Comprar Cortinas Blackout, Persiana para sala e Cortina para sala nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Persiana Painel em Cocaia - Guarulhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>