<?php
$title       = "Persianas em Alumínio em Jardim Tranquilidade - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Persianas em Alumínio em Jardim Tranquilidade - Guarulhos são muito duráveis, higiênicas e de fácil manutenção. Feitas em material resistente e que bloqueia os raios solares, proporciona um ambiente com iluminação controlada e perfeitas para privacidade, modernidade e conforto. Com a Maliete Decorações você encontra persianas em modelos de alumínio, pvc, entre outros materiais, tecidos, cores e texturas. Conheça mais sobre nossos produtos.</p>
<p>A Maliete Decorações é uma empresa que se destaca no setor de  Cortinas, Persianas, Papel de Parede e Tapeçarias, pois oferece não somente Cabeceira para Cama de Casal, Persiana Iluminê, Onde Comprar Cortinas Blackout, Manutenção de Persianas e Papel de Parede para Lavabo, mas também, Persianas em Alumínio em Jardim Tranquilidade - Guarulhos com o intuito de atender às mais exigentes solicitações. Entre em contato e faça uma cotação com um de nossos competentes profissionais e assim comprove que somos a empresa que trabalha com o foco na qualidade e satisfação de nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>