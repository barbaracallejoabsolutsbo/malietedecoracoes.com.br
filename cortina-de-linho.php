<?php
    $title       = "Cortina de linho";
    $description = "A cortina de linho oferece ótimo aproveitamento da luz natural, sendo utilizada em locais como halls, sala de jantar ou salas de estar, por exemplo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Maliete Decorações está há mais de 30 anos no mercado e tem como objetivo trazer a melhor qualidade não só na <strong>cortina de linho,</strong> mas em todos os tipos de cortinas, persianas e papeis de parede do nosso portfólio, transformando seu lar. Nosso foco é proporcionar aos nossos clientes os melhores produtos de decoração interior, deixando seu espaço bonito e confortável.</p>
<p>Todos os nossos produtos, desde a <strong>cortina de linho</strong> até persiana e papeis de parede são feitos sob medida. Tudo é pensado para que sua casa, escritório, quarto, sala, ou qualquer ambiente, seja um local com seu estilo e personalidade.</p>
<p>Estamos localizados em São Paulo, com dois locais de atendimento – Av. Timóteo Penteado, 4504 ou Rua Emília Marengo, 09 - e para melhor atendimento estamos trabalhando com hora marcada. Nossos profissionais possuem experiência e conhecimento não só a respeito da <strong>cortina de linho,</strong> mas em todos os nosso demais produtos.</p>
<p>Quando falamos de decoração, as janelas devem ter um cuidado específico, já que seu manuseio exerce grandes mudanças e impacto nos interiores. Cada espaço possui suas necessidades e funcionalidades, portanto, combinar a beleza estética é fundamental para torna-lo mais agradável, e assim, mais funcional.</p>
<p>A Maliete Decorações se preocupa não só com a beleza de seus espaços, mas também com seu bem-estar. Por isso, falar de decoração é muito mais do que apresentar objetos bonitos para um local agradável. Nosso trabalho é viabilizar os melhores produtos capazes de exercer sua melhor função, tudo isso com beleza, responsabilidade e profissionalismo.</p>
<h2>Tudo sobre a <strong>cortina de linho</strong></h2>
<p>As cortinas, além de decorativas, são objetos diretamente relacionas com a iluminação do ambiente. A <strong>cortina de linho</strong> proporciona uma atmosfera elegante e discreta, seja em salas, quartos ou escritórios, pode ser utilizada para valorizar o espaço e suas decorações. </p>
<p>Em suas cores e tons pastéis, principalmente, não é a toa que esse tipo de material é escolhido com certa preferencia quando o assunto é confecções de cortinas. A iluminação é ponto importante quando falamos de decoração e interiores, sendo sua transparência, maior ou menor, o toque que levará charme quando o assunto é <strong>cortina de linho</strong>.</p>
<p>Exatamente pela sua característica de transparência, <strong>a cortina de linho</strong> oferece ótimo aproveitamento da luz natural, sendo utilizada em locais que a privacidade não é prioridade, como halls, sala de jantar ou salas de estar, por exemplo. Já em quartos, pode não ser a melhor opção.</p>
<p>No aspecto estético, <strong>a cortina de linho </strong>traz caimento, textura e fluidez, trazendo sensações de conforto e aconchego. Com seu aspecto mais arenoso, a <strong>cortina de linho</strong> combina com móveis e decorações coloridas, ressaltando detalhes do ambiente. A delicadeza da <strong>cortina de linho </strong>resulta em facilidade e versatilidade na hora de usá-la.</p>
<p>O tecido <strong>da cortina de linho</strong> é um dos tecidos de decorações mais procurados para aplicar suavidade no ambiente, podendo ser utilizado sozinho ou combinado com outros tipos de tecido. A <strong>cortina de linho </strong>pode ser combinada com tecido voil, seda, entre outros, o que pode conferir ainda mais charme à <strong>cortina de linho.</strong></p>
<p>A escolha da <strong>cortina de linho </strong>e sua combinação vão depender do aspecto que você quer proporcionar para o local. São diferentes as sensações e movimentos que o ambiente irá adquirir. Por exemplo, combinada com blecaute, permite maior controle da luminosidade.</p>
<p>Se o material for leve e transparente, comum para esse tipo de cortina, a combinação com forro e outros tecidos é muito utilizada, principalmente para cômodos privativos, como os quartos.</p>
<h3><strong>Cortina de linho</strong>: simples e versátil</h3>
<p>As cortinas de linho se encaixam até em ambientes mais sofisticados e elegantes quando a assunto é decoração. Sua atmosfera acolhedora e simples evidencia os objetos e móveis do ambiente.</p>
<p> Decoração e identidade estão intimamente ligadas. Seu estilo também reflete em suas escolhas decorativas, colocando personalidade em seus ambientes. Isso quando falamos de roupas, acessórios, móveis, utensílios domésticos... e a decoração não fica de fora. Pois isso, escolher o melhor tipo de cortina para você e cada ambiente, é uma dica especial de nossa loja. Confira dois tipos de <strong>cortina de linho </strong>que estão em nosso portfólio:</p>
<p>- Cortina de Gaze Linho feita para uma sacada.</p>
<p>- Cortina de Linho com um alto pé direito e 3x de franzido.</p>
<p>Se você ainda tem dúvidas de como utilizar a <strong>cortina de linho</strong> da melhor maneira possível, conheça nossa loja e converse com um de nossos funcionários. A boa notícia é que a <strong>cortina de linho</strong> combina basicamente com qualquer estilo de decoração, do simples ao mais moderno e sofisticado.</p>
<p>Conhecendo mais sobre a <strong>cortina de linho</strong>, saiba que nossa equipe trabalha com materiais de primeira linha independente do projeto que está sendo feito. Tudo é pensado para que você tenha a melhor experiência e o melhor resultado decorativo, com um ambiente que seja sua cara!</p>
<p>A decoração de interiores vem para promover diferentes sensações e consagrar o melhor aproveitamento do espaço. Essa ideia desperta aconchego, produtividade e proporciona melhores ambientes para lazer, trabalho, convivência, e assim por diante.</p>
<p>Nossa equipe esta sempre estudando e se adaptando as mudanças dessa área, para que você, nosso cliente, receba o máximo de conforto e qualidade em nossos produtos. Trabalhamos para que os preços de nossos produtos sejam acessíveis e compatíveis com seu bolso, para que você possa adquiri-los no momento em que desejar e precisar.  </p>
<p>Consideramos nossos clientes parte de nossa história, por isso, cada projeto é único e especial para nós. Priorizamos a qualidade e o atendimento de nossos serviços para que todos os nossos clientes tenham o resultado que desejam e seu espaço bem decorado.</p>
<p>Assim, para que você tenha a melhor experiência de compra, entre em contato com nossa loja e conheça um de nossos profissionais. Ele estará disponível para que dúvidas sejam sanadas sobre a nossa <strong>cortina de linho </strong>ou qualquer outro produto que seja do seu interesse. Garantimos o atendimento que você procura a qualquer momento que nos contatar.</p>
<p>Faça o seu orçamento através do nosso Whatsapp e qualquer dúvida não hesite! Estamos prontos para melhor atende-los.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>