<?php
$title       = "Cortina sob medida na Vila Curuçá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao encomendar uma Cortina sob medida na Vila Curuçá você está solicitando um produto exclusivo feito pensando especialmente em você de acordo com seus gostos e a medida necessária que você precisa. Atuamos também com outros modelos de cortinas que podem ser conferidos através do catálogo virtual disponível em nosso site. Caso tenha quaisquer dúvidas entre em contato e seja auxiliado por nossa equipe.  </p>
<p>A Maliete Decorações, como uma empresa em constante desenvolvimento quando se trata do mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias visa trazer o melhor resultado em Cortina sob medida na Vila Curuçá para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Persiana Blackout para quarto, Onde Comprar Cortinas Blackout, Persiana Celular, Persiana Passione e Manutenção de Persianas para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>