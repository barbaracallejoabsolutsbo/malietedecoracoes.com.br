<?php
$title       = "Persiana Celular na Cidade Dutra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Persiana Celular na Cidade Dutra que você tanto procura está aqui a poucos cliques de você na Maliete Decorações. Nossa loja atua há mais de 30 anos com persianas, cortinas, papeis de parede e tapeçaria de alto padrão. Trabalhamos com a proposta de oferecer produtos de alta qualidade com preços justos e condições de pagamento que cabem no seu bolso. Visite nosso catálogo e confira as opções. </p>
<p>Procurando uma empresa de confiança onde você possa garantir o melhor em  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações é a companhia certa. Aqui desde Persiana Celular na Cidade Dutra até Persiana para escritório, Persiana para quarto, Persiana para sacada, Cortina de Tecido e Persiana Vertical é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções em energia sustentável. Entre em contato conosco e saiba mais!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>