<?php
$title       = "Loja de Persiana Motorizada no Bom Retiro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nossa Loja de Persiana Motorizada no Bom Retiro você encontra o que há de mais moderno e de qualidade no mercado de cortinas, persianas e papéis de parede. Especializados em decoração de interiores, oferecemos infinitas opções de tecidos, materiais, texturas e cores para projetos de persiana motorizada do seu gosto. Conheça mais sobre a Maliete Decorações e faça seu projeto de persiana motorizada conosco.</p>
<p>Nós da Maliete Decorações estamos entre as principais empresas qualificadas no ramo de  Cortinas, Persianas, Papel de Parede e Tapeçarias. Temos como principal missão realizar uma ótima assessoria tanto em Loja de Persiana Motorizada no Bom Retiro, quanto à Persianas em Alumínio, Loja de Papel de Parede para Banheiro, Papel de parede de linho, Cortina de Forro e Voil e Persiana Romana, uma vez que, contamos com profissionais qualificados e prontos para realizarem um ótimo atendimento. Entre em contato conosco e tenha a satisfação que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>