<?php
$title       = "Cortinas de Trilho na Vila Formosa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Cortinas de Trilho na Vila Formosa são geralmente instaladas com a técnica trilho suíço, que mescla o uso de sancas de gesso para que as cortinas adentrem o teto. Proporcionando design único, moderno e luxuoso, este e outros modelos de cortinas e persianas estão disponíveis com a Maliete Decorações. Nossa empresa oferece suporte exclusivo para decoração de interiores com papéis de parede, cortinas e persianas, veja mais.</p>
<p>Com sua credibilidade no mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias, proporcionando com qualidade, viabilidade e custo x benefício seja em Cortinas de Trilho na Vila Formosa quanto em Papel de parede para sala, Persiana para sala, Cortinas para Sala Preço, Persiana cinza e Onde Comprar Canto Alemão, podendo dessa forma garantir seu sucesso no segmento em que atua de forma idônea e com altíssimo nível de qualidade a Maliete Decorações é a opção número um para você garantir o melhor no que busca!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>