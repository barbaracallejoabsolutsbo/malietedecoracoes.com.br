<?php
    $title       = "Cortina de voil";
    $description = "Nosso trabalho é viabilizar os melhores produtos capazes de exercer sua melhor função, tudo isso com beleza, responsabilidade e profissionalismo";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Maliete está há mais de 30 anos no mercado e tem como objetivo trazer a melhor qualidade não só na <strong>cortina de voil,</strong> mas em todos os tipos de cortinas, persianas e papeis de parede do nosso portfólio, transformando seu lar. Nosso foco é proporcionar aos nossos clientes os melhores produtos de decoração interior, deixando seu espaço bonito e confortável.</p>
<p>Todos os nossos produtos, desde a <strong>cortina de voil</strong> até persiana e papeis de parede são feitos sob medida. Tudo é pensado para que sua casa, escritório, quarto, sala, ou qualquer ambiente, seja um local com seu estilo e personalidade.</p>
<p>Estamos localizados em São Paulo, com dois locais de atendimento – Av. Timóteo Penteado, 4504 ou Rua Emília Marengo, 09 - e para melhor atendimento estamos trabalhando com hora marcada. Nossos profissionais possuem experiência e conhecimento não só a respeito da <strong>cortina de voil,</strong> mas em todos os demais produtos.</p>
<p>Quando falamos de decoração de interiores, as janelas devem ter um cuidado específico, já que seu manuseio exerce grandes mudanças e impacto nos interiores. Cada espaço possui suas necessidades e funcionalidades mais específicas, portanto, combinar a beleza estética é fundamental para torna-lo mais agradável, e assim, mais funcional.</p>
<p>A Maliete se preocupa não só com a beleza de seus espaços, mas também com seu bem-estar. Por isso, falar de decoração é muito mais do que apresentar objetos bonitos para um local agradável. Nosso trabalho é viabilizar os melhores produtos capazes de exercer sua melhor função, tudo isso com beleza, responsabilidade e profissionalismo.</p>
<h2>Tudo sobre a <strong>cortina de voil</strong></h2>
<p>A iluminação é ponto importante quando o assunto é decoração e interiores. Isso por que um lugar mal iluminado ou com iluminação inadequada pode causar danos à saúde, despesas adicionais e uso exagerado de energia elétrica.</p>
<p>As cortinas, além de decorativas, são objetos diretamente relacionos com a iluminação ambiental. A <strong>cortina de voil</strong> proporciona uma atmosfera mais discreta e elegante para seu ambiente, seja em salas, quartos ou escritórios, pode ser utilizada para valorizar o espaço e suas decorações. </p>
<p>Com excelente custo benefício, <strong>a cortina de voil</strong> oferece ótimo aproveitamento da luz natural, levando privacidade para seus interiores sem deixar o ambiente com aspecto pesado, ou seja, possibilitando maior conforto para você.</p>
<p>Além do aspecto estético, <strong>a cortina de voil</strong> é prática já que pode ser lavada em máquina e secada com facilidade. A leveza da <strong>cortina de voil</strong> permite rápida lavagem e secagem, e a delicadeza dessa <strong>cortina de voil</strong> resulta em facilidade e versatilidade na hora de usá-la.</p>
<p>O tecido <strong>da cortina de voil</strong> é um dos tecidos de decorações mais procurados e que aplicam suavidade no ambiente, podendo ser utilizado sozinho ou combinado com outros tipos de tecido. A <strong>cortina de voil</strong> é feita com fios de trama altamente torcidos e de baixa densidade, podendo ser encontrada em diversas cores tons e texturas. O que confere ainda mais charme é a <strong>cortina de voil</strong> em cores estampadas.</p>
<p>Bordada, lisa, vazada ou estampada, a <strong>cortina voil</strong> pode ser combinada com outros tipos de tecidos, dependendo do aspecto que você quer proporcionar para o local. Por exemplo, combinada com blecaute, permite maior controle da luminosidade.</p>
<p>Esse tipo de cortina pode ser confeccionada a partir de algodão ou mesclas de poliéster, sendo duradouras e de ótimo custo benefício. Dependendo da escolha de estampas e cores, diferentes movimentos e sensações serão transmitidas para o ambiente, sendo sua versatilidade uma de suas melhores vantagens.</p>
<p>Se o material for leve e transparente, comum para esse tipo de cortina, a combinação com forro e outros tecidos é muito utilizada, principalmente para cômodos privativos, como os quartos.</p>
<h3>Nossos tipos de <strong>cortina de voil</strong></h3>
<p>Decoração e identidade estão intimamente ligadas. Nosso estilo também reflete em nossas escolhas decorativas, colocando personalidade nos ambientes em que estamos. Isso quando falamos de roupas, acessórios, móveis, utensílios domésticos... e a decoração não fica de fora. Pois isso, escolher o melhor tipo de cortina para você e cada ambiente, é uma dica especial de nossa loja. Confira:</p>
<p><strong>Cortina de Voil</strong> Leda:</p>
<p>- Cortina no modelo wave com voil leda e barra simples.<br /> - Cortina de 5,15m de comprimento feita de voil Leda, com barra dupla e acabamento wave.<br /> - <strong>Cortina de voil</strong> Leda com 3x de franzido no tecido</p>
<p><strong>Cortina de voil</strong> com chale no quarto:</p>
<p>- <strong>Cortina de voil</strong> Judith no quarto com chale listrado.</p>
<p>Cortina para sala voil liso e chale:</p>
<p>- Cortina de sala em voil liso branco e chale em chantum embutidos com trilho suiço na sanca</p>
<p>Agora que você conhece um pouco mais sobre a <strong>cortina de voil</strong>, saiba que nossa equipe trabalha com materiais de primeira linha independente do projeto que está sendo feito. Tudo é pensado para que você tenha a melhor experiência e o melhor resultado decorativo, com um ambiente que seja sua cara!</p>
<p>A decoração de interiores vem para promover diferentes sensações e consagrar o melhor aproveitamento do espaço. Essa ideia desperta aconchego, produtividade e proporciona melhores ambientes para lazer, trabalho, convivência, e assim por diante.</p>
<p>Nossa equipe esta sempre estudando e se adaptando as mudanças dessa área, para que você, nosso cliente, receba o máximo de conforto e qualidade em nossos produtos. Trabalhamos para que os preços de nossos produtos sejam acessíveis e compatíveis com seu bolso, para que você possa adquiri-los no momento em que desejar e precisar.  </p>
<p>Consideramos nossos clientes parte de nossa história, por isso, cada projeto é único e especial para nós. Priorizamos a qualidade e o atendimento de nossos serviços para que todos os nossos clientes tenham o resultado que desejam e seu espaço decorado.</p>
<p>Assim, para que você tenha a melhor experiência de compra, entre em contato com nossa loja e conheça um de nossos profissionais. Eles estarão estará disponíveis para que suas dúvidas sejam sanadas sobre a nossa <strong>cortina de voil </strong>ou qualquer outro produto que seja do seu interesse. Garantimos o atendimento que você procura a qualquer momento que nos contatar.</p>
<p>Faça o seu orçamento através do nosso Whatsapp e qualquer dúvida não hesite! Estamos prontos para melhor atende-los.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>