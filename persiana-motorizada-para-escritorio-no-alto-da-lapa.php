<?php
$title       = "Persiana Motorizada para Escritório no Alto da Lapa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Confeccionadas pela Maliete Decorações, a Persiana Motorizada para Escritório no Alto da Lapa pode contar com diversos modelos, cores e texturas. Há mais de 30 anos no mercado, nossa empresa oferece o que há de melhor no mercado de persianas e cortinas. Especializados em decoração de interiores, solicite orçamentos para produtos e serviços de instalação, reparo e reforma de cortinas e persianas para seu ambiente.</p>
<p>Além de sermos uma empresa especializada em Persiana Motorizada para Escritório no Alto da Lapa disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Persiana Painel, Loja de Cabeceira para Cama de Solteiro, Persiana Motorizada Preço, Papel de parede para sala e Motorização de Cortinas. Com a ampla experiência que a equipe Maliete Decorações possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de  Cortinas, Persianas, Papel de Parede e Tapeçarias.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>