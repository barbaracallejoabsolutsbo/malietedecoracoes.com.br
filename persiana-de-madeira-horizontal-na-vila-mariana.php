<?php
$title       = "Persiana de madeira horizontal na Vila Mariana";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você procura por uma Persiana de madeira horizontal na Vila Mariana com alta qualidade e preço justo chegou ao lugar certo. Seja bem-vindo a Maliete Decorações, somos uma loja que atua com persianas, cortinas, papeis de parede e tapeçaria de alta qualidade há mais de 30 anos. Com qualidade e compromisso no atendimento conquistamos o posto de referência dentre as lojas do segmento no Brasil. </p>
<p>Tendo como especialidade Papel de parede para sala, Cortina de linho, Loja de Cabeceira para Cama de Solteiro, Onde Comprar Canto Alemão e Cortinas de Trilho, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de  Cortinas, Persianas, Papel de Parede e Tapeçarias. Por isso, quando falamos de Persiana de madeira horizontal na Vila Mariana, buscar pelos membros da empresa Maliete Decorações é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>