<?php
$title       = "Persiana Painel em Anália Franco";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você ainda não conhece a Persiana Painel em Anália Franco, este modelo é inovador e moderno que proporciona um design diferenciado para grandes vãos. Composta por painéis com larguras que se sobrepõem para cobrir por completo quando esticadas, são perfeitas para grandes janelas e painéis de vidro. Conheça os serviços especializados em decoração de interiores da Maliete Decorações. Fale conosco e conheça mais.</p>
<p>Na busca por uma empresa referência, quando o assunto é  Cortinas, Persianas, Papel de Parede e Tapeçarias, a Maliete Decorações será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Papéis de Parede, Modelo de Cortinas para Quarto, Cortinas de Trilho, Cabeceira de Cama Sob Medida e Papel de Parede para Lavabo, oferece Persiana Painel em Anália Franco com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>