<?php
$title       = "Cortinas para Sala Preço em São Bernardo do Campo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre diversos modelos de Cortinas para Sala Preço em São Bernardo do Campo baixo e alta qualidade. A Maliete Decorações está no mercado há mais de 30 anos oferecendo o que há de melhor em cortinas, persianas e papéis de parede. Especializados em decoração de interiores, oferecemos mão de obra especializada para instalação, manutenção, reparos e reformas em todos estes itens acima citados. Conheça mais sobre nós.</p>
<p>Se você está em busca de Cortinas para Sala Preço em São Bernardo do Campo tem preferência por uma empresa com conhecimento e custo-benefício, a Maliete Decorações é a melhor opção para você. Com uma equipe formada por profissionais amplamente qualificados e dedicados para oferecer soluções personalizadas para cada cliente que busca pela excelência. Entre em contato com a gente e saiba mais sobre  Cortinas, Persianas, Papel de Parede e Tapeçarias e todos os nossos serviços como Loja de Cabeceira para Cama de Solteiro, Papéis de Parede, Persiana Lumiére, Cortinas de Trilho e Cortina para Teto.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>